﻿# -*- coding: euc-kr -*-

'''
Created on 2012. 5. 4.
utf-8 save
@author: jin1
'''

###########################################################################
## Python code generated with wxFormBuilder (version Apr 10 2012)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.grid

###########################################################################
## Class MyFrame2
###########################################################################


class MyFrame2 ( wx.Frame ):
    
    def __init__( self, parent ):
        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u" sqlite db viwer", pos = wx.DefaultPosition, size = wx.Size( 737,463 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
        
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        
        bSizer11 = wx.BoxSizer( wx.HORIZONTAL )
        
        self.m_panel3 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer12 = wx.BoxSizer( wx.VERTICAL )
        
        self.grid21 = wx.grid.Grid( self.m_panel3, wx.ID_ANY, wx.DefaultPosition, wx.Size( 640,300 ), 0 )
        
        # Grid
        self.grid21.CreateGrid( 50, 23 )
        self.grid21.EnableEditing( True )
        self.grid21.EnableGridLines( True )
        self.grid21.EnableDragGridSize( False )
        self.grid21.SetMargins( 0, 0 )
        
        # Columns
        self.grid21.EnableDragColMove( False )
        self.grid21.EnableDragColSize( True )
        self.grid21.SetColLabelSize( 30 )
        self.grid21.SetColLabelValue( 0, u"처방일" )
        self.grid21.SetColLabelValue( 1, u"검사일시" )
        self.grid21.SetColLabelValue( 2, u"차트번호" )
        self.grid21.SetColLabelValue( 3, u"이름" )
        self.grid21.SetColLabelValue( 4, u"검체번호" )
        self.grid21.SetColLabelValue( 5, u"진료과" )
        self.grid21.SetColLabelValue( 6, u"내원번호" )
        self.grid21.SetColLabelValue( 7, u"WBC" )
        self.grid21.SetColLabelValue( 8, u"RBC" )
        self.grid21.SetColLabelValue( 9, u"Hb" )
        self.grid21.SetColLabelValue( 10, u"Hct" )
        self.grid21.SetColLabelValue( 11, u"MCV" )
        self.grid21.SetColLabelValue( 12, u"MCH" )
        self.grid21.SetColLabelValue( 13, u"MCHC" )
        self.grid21.SetColLabelValue( 14, u"Platelet" )
        self.grid21.SetColLabelValue( 15, u"Neutrophil" )
        self.grid21.SetColLabelValue( 16, u"Lymphocyte" )
        self.grid21.SetColLabelValue( 17, u"Monocyte" )
        self.grid21.SetColLabelValue( 18, u"PDW" )
        self.grid21.SetColLabelValue( 19, u"RDW" )
        self.grid21.SetColLabelValue( 20, u"PCT" )
        self.grid21.SetColLabelValue( 21, u"MPV" )
        self.grid21.SetColLabelValue( 22, u"Raw Data" )
        self.grid21.SetColLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
        
        # Rows
        self.grid21.EnableDragRowSize( True )
        self.grid21.SetRowLabelSize( 40 )
        self.grid21.SetRowLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
        
        # Label Appearance
        
        # Cell Defaults
        self.grid21.SetDefaultCellAlignment( wx.ALIGN_LEFT, wx.ALIGN_TOP )
        bSizer12.Add( self.grid21, 1, wx.ALL|wx.EXPAND, 5 )
        
        
        self.m_panel3.SetSizer( bSizer12 )
        self.m_panel3.Layout()
        bSizer12.Fit( self.m_panel3 )
        bSizer11.Add( self.m_panel3, 1, wx.EXPAND |wx.ALL, 5 )
        
        
        self.SetSizer( bSizer11 )
        self.Layout()
        self.m_statusBar1 = self.CreateStatusBar( 1, wx.ST_SIZEGRIP, wx.ID_ANY )
        
        self.Centre( wx.BOTH )
    
    def __del__( self ):
        pass
    

