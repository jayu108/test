# -*- coding: euc-kr -*-


'''
Created on 2012. 6. 17.

@author: jin1
'''



import labdesign
import serialadd1
import serialadd2
import serialtxt
import machinsel
import mainf
import dbsetting
import chemdialog

import wx
import wx.grid
import pyodbc
import datetime
import time
import threading
import serial
import sqlite3
import operator
import hashlib
import logging


COMPORT = ['COM1', 'COM2', 'COM3','COM4','COM5','COM6','COM7','COM8','COM9']

STX = '\x02'
ETX = '\x03'
ETB = '\x17'   # max = 240자, 넘으면  <ETB> 로 frame 분리.

ENQ = '\x05'
ACK = '\x06'
NAK = '\x15'  # Not acknowledge
EOT = '\x04'  # end of transmission


class Form1 ( labdesign.MyForm ):
    def __init__(self,parent,labmode):
        labdesign.MyForm.__init__(self,parent=None)

        self.labmode = labmode

        # print 'lab mode = ', self.labmode    # 장비모드 , 'cbc', 'electro','chem', etc..

#        print 'titile ==== ' , self.Title

        if (self.labmode == 'cbc'):
            self.Title = 'CBC 장비'
        elif(self.labmode == 'electro'):
            self.Title = 'Electrolyte 장비'
        elif(self.labmode =='chem'):
            self.Title = '생화학 검사 장비'


        self.RS232Viwerflag  = False  #  RS232 text viewer flag초기화.     #클래스멤버

        self.MyStart()




    # Virtual event handlers, overide them in your derived class

    def OnCloseWindows( self, event ):  # main frame 종료

        self.sqconn.close()  # main thread sqlite db close!

        self.th1evt.clear()  # th1 thread의 loop를 빠져나온후에...
        self.th1.join()    # thread 종료후                  # 1초후에도 daemon thread 종료되지 않더라도.. 강제로 join() 하라!!  *******
        self.Destroy()    # 프로그램 종료!


    def getDays(self,startdate,enddate):   # 검색 시작일, 종료일 구하기.
        m1 = startdate.FormatISODate().split('-')
        m2  = enddate.FormatISODate().split('-')
        startday=''.join(m1)
        endday = ''.join(m2)

        dday = int(endday)-int(startday)

        if (dday < 0):  # 날짜 순서 잘못 선택시..
#            print ' abnormal range date!!!!!    날짜 선택 다시 하시요!!'
            Msg = serialadd2.MyDialog1(None)
            frame.Iconize(True)
            result = Msg.ShowModal()

            if result == wx.ID_OK: Msg.Destroy()
#            else: errdialog1.Destroy()
            return None, None

        startday = startday+'0000'     #YYYYMMHH0000  -- 00시 00분
        endday = endday +'2359'      # YYYYMMHH2359  -- 23시 59분

        return startday, endday


    def makeQuery(self,startsql,endsql,colname, datalist):
        sql = startsql + '('

        for code in datalist :
            sql = sql + colname+ '\'' +  code + '\'' + ' or '

        sql =  sql[:-3] +') '    #   sql[:-4]  --->  마지막  ' and '  제거
        sql = sql + endsql
        return sql


    def maketmpid(self,check,oderdate, chtno,name,examstatus,ocmnum,dept):
#        tmpid=[]     # rectmp 비어있으면, 즉 처음시작시
#        tmpid.append('1')   # check
#        tmpid.append(rec[0])  # 처방일
#        tmpid.append(rec[1])    # 차트번호
#        tmpid.append(rec[2])     # name
#        tmpid.append('')   # 진행상황
#        tmpid.append(rec[4])  # ocmnum
#        tmpid.append(rec[5])  # 진료과
        tmpid = [check,oderdate, chtno,name,examstatus,ocmnum,dept ]
        return tmpid


    def OnBtnSearch( self, event ):

        d1 = self.date1.GetValue()   # 상속받음...  self.date1 = wx.DatePickerCtrl
        d2 = self.date2.GetValue()

        # print 'self.date1  ============================ ' , self.date1

        startday, endday = self.getDays(d1, d2)   # 검색 시작일, 종료일 구하기.

        if (startday == None) : return  # 날짜 범위 잘못지정시 그냥 method 종료...

        datalist = [ ocscode for ocscode, name in self.labcodedata]

#        startsql = """select   o.ospodrdtm, o.OspChtNum,p.pbspatnam, o.ospchkstt,o.OspOcmNum,o.OspUidPrt, o.ospodrcod, o.ospodrseq,o.OspDelFlg
#                        from OspInf o inner join PbsInf p
#                        on o.ospchtnum = p.pbschtnum
#                        inner join SeeMst c
#                        on o.ospodrcod = c.seeodrcod
#                        where  o.ospodrdtm >= '%s'  and o.ospodrdtm <= '%s' and c.seeexpdte ='99999999' and o.ospcsmdep ='LAB' and o.OspDelFlg ='N' and o.OspChkStt = 'A'
#                        and o.OspUidPrt in ('MD', 'MED','MED1')
#                        and  """
#
#
#        endsql =  """order by o.ospodrdtm, o.OspOcmNum, o.OspOdrSeq"""
#        colname = "o.OspOdrCod="
#        sql = self.makeQuery(startsql%(startday,endday), endsql, colname, datalist)

        startsql2 = """select os.ospodrdtm, os.OspChtNum, p.pbspatnam, os.ospchkstt,os.OspOcmNum,os.OspUidPrt, os.ospodrcod, os.ospodrseq,os.OspDelFlg from OspInf os
                        inner join pbsinf p
                        on os.OspChtNum = p.PbsChtNum
                        where  os.OspOcmNum in (
                            select o.ocmnum from OcmInf o
                            where  o.ocmacpdtm  >= '%s' and o.ocmacpdtm <= '%s'
                            )
                            and  os.ospcsmdep ='LAB' and os.OspDelFlg ='N' and os.OspChkStt = 'A'
                            and  """

        endsql2 =  """order by os.ospodrdtm, os.OspOcmNum, os.OspOdrSeq"""
        colname2 = "os.OspOdrCod="


        sql2 = self.makeQuery(startsql2%(startday,endday), endsql2, colname2, datalist)

        # print 'search sql  = ', sql
        print 'search sql2  = ', sql2

        self.cursor.execute(sql2)
        grid1recs2 = self.cursor.fetchall()      #  개별 order line 을 검색한 결과   --> 이결과들을 환자별, 과별, comnum 별로 정리하여 self.grid1List2를 만든다!

        # print 'search grid1recs2 =' , grid1recs2

        rectmp =[] # 임시저장, rectmp[0] = ocmnum, 나머지는 (labcode, oder line No) tupule
        # 예:  rectmp = ['    169617', ['B1050     ', 28], ['B1040     ', 29], ['B1010     ', 30], ['B1020     ', 31], ['B1060     ', 32], ['B1220     ', 33], ['B1230     ', 34]]

        tmpid=[]  # [0] ; check,    [1]; 처방일,    [2]; 차트번호,     [3]; 이름,     [4]; 상태,       [5];ocmnum,    [6]; 진료과

        self.grid1List2=[]   # gird1 에  표시할 환자정보, 검사코드, order 번호 정보 (기초정보)
                                # 여기에 검사결과 등을 추가하여 self.grid1List3 만든다.

        for rec in grid1recs2:  # 개별 order line 을  정리하여, 환자별로 정리하여 self.grid1List2를 만든다!
        # rec[0] ; 처방일시,    rec[1] ; 차트번호,   rec[2] ; 이름,   rec[4]; ocmnum,   rec[5]; 진료과,   rec[6] ; labcode,   rec[7] ; 오더순서
        #
        #  1) 처음시작시
        #  2) - <1> ocmnum 바뀐 경우 --> 다른 환자
        #     - <2> 진료부서(dept) 바뀐 경우  ---> 같은 환자라도 다른 부서 오더는 따로 처리..
        #     - <3> 중복오더   -- 같은 환자 다른 검사 (ex: 같은날 CBC f/u 오더 있는 경우)
        #
            # print 'rec ===', rec
            if(rectmp):
                if(rectmp[0][5] != rec[4]):   # ocmnum 가 다르면, 다른 환자 검사이므로
                    self.grid1List2.append(rectmp)  # 이제까지의 결과를 self.grid1List2에 저장하고....
                    rectmp= []
                    tmpid = self.maketmpid('1', rec[0], rec[1], rec[2], '', rec[4], rec[5])    # 상태는 일단 ''로 표시하고, 아래에서 '검사대기','검사완료','부분완료' 로 바꾼다.
                    rectmp.append(tmpid)   # rectmp 비어있으면, 즉 처음시작시
            else:    # rectmp 비어있으면, 즉 처음시작시
                tmpid = self.maketmpid('1', rec[0], rec[1], rec[2], '', rec[4], rec[5])
                rectmp.append(tmpid)   # rectmp 비어있으면, 즉 처음시작시

            if(len(rectmp)>1):   # 예 :  rectmp = [['1', '201207231200', '      00025789', '\xb1\xe8\xb0\xc7\xc7\xfc', '', '    173304', 'MD   '], ['B1010     ', 14]]
#                print 'rectmp ---->>>> ', rectmp
                for i, data in enumerate(rectmp):
#                    print i, data, data[0]
                    if (i==0):
                        if(data[6] != rec[5]):   # rectmp 에 저장된 dept 부서(data[6])가  현재의 dept (rec[5]) 와 다르다면 , 새로운 rectmp 만들라!
                            self.grid1List2.append(rectmp)
                            rectmp= []
                            tmpid = self.maketmpid('1', rec[0], rec[1], rec[2], '', rec[4], rec[5])
                            rectmp.append(tmpid)   # rectmp 비어있으면, 즉 처음시작시
                    else:
                        if(data[0] == rec[6]):  # rectmp 에 저장된 labcode (data[0]) 가  현재의 labcode(rec[6]) 와 겹치면, 새로운 rectmp 를 만들어 저장하라.
                            self.grid1List2.append(rectmp)
                            rectmp= []
                            tmpid = self.maketmpid('1', rec[0], rec[1], rec[2], '', rec[4], rec[5])
                            rectmp.append(tmpid)   # rectmp 비어있으면, 즉 처음시작시

            tmp = []  # labcode, orderline No  저장 ..(추후 검사 결과추가 저장한다!!!!!)
            tmp.append(rec[6])    # rec[6] ; labcode,   rec[7] ; 오더순서
            tmp.append(rec[7])
            rectmp.append(tmp)

        if(rectmp) : # 마지막 rectmp 에 data가 있으면
            self.grid1List2.append(rectmp)    #마지막 record 저장하기.



        self.grid1List3 =[]  # 초기화,    쿼리 결과  grid1recs2 에  cbc결과, 진행상황등을 추가하여 grid1 data 완성한 List!!!!

        if(self.grid1List2):  # self.grid1List3 만드는 부분
            for i,rec in enumerate(self.grid1List2):
                startsql = """ select  ResLabCod,ResOdrSeq, ResRltVal  from ResInf
                                    where resocmnum = '%s'
                                and """
                datalist = [str(data[1]) for k, data in enumerate(rec) if k>0]   # order번호만 추출하여 list 만든다!
                colname = ' ResOdrSeq = '
#                print 'datalist ===', datalist
                sql = self.makeQuery(startsql%rec[0][5], '', colname, datalist)    # rec[0][5]  == ocmnum
#                print 'sql ====', sql
                self.cursor.execute(sql )
                labresult = self.cursor.fetchall()    # labresult ; ('cbc코드', 'order번호', '결과값') 으로 구성된 tupule 의 List

                tmp2 = [data[2] for data in labresult]    # labresult중 결과값 임시저장

                # '' 로 표시된 상태항목을  '검사대기','검사완료','부분완료' 로 바꾼다.
                if(tmp2.count(None)==0):
                    rec[0][4] = '검사완료'
                elif(len(tmp2) == tmp2.count(None)):
                    rec[0][4] = '검사대기'
                else:
                    rec[0][4] = '부분완료'

                self.grid1List3.append([rec[0]]+labresult)   # grid1 데이터 집어넣기 ,  [rec[0]] == rec[0] 리스트에 list 한번더 쒸우기
                                                                    # rec[0] == 환자정보



        if(self.RbList.GetSelection() == 0):             # 검사완료자제외 선택시....
            for i, rec in reversed(list(enumerate(self.grid1List3))):    # 역순으로  뒤에것부터 삭제하기 위함.
#                print i, rec
                if (rec[0][4] =='검사완료'):
#                    print rec[0][4]
                    del self.grid1List3[i]   # 검사완료자 delete!

#        print ' new   self.grid1List3 = ', self.grid1List3  # '검사완료자제외' 선택시에는 부분완료, 검사대기 만 포함된다...


        recsCount = len(self.grid1List3)   # records count


        self.gridPrepRow(self.grid1,recsCount)


        for i, rec in enumerate(self.grid1List3):            # grid1 에  데이터 표시하기!!!!
#            print rec[0][0], rec[0][1], rec[0][2], rec[0][3],rec[0][4],rec[0][5], rec[0][6]
#            print i, rec
            self.grid1.SetCellValue(i,0,rec[0][0])   # check 표시
            self.grid1.SetCellValue(i,1,rec[0][1][:8])  # 처방일만 표시  ---   'YYYYMMDDHHMM' 중 앞 8자만 표시..
            self.grid1.SetCellValue(i,2,rec[0][2])
            self.grid1.SetCellValue(i,3,rec[0][3])
            self.grid1.SetCellValue(i,4,rec[0][4])
            self.grid1.SetCellValue(i,5,rec[0][5])     #  rec[4] --> ocmnum 내원번호
            self.grid1.SetCellValue(i,6,rec[0][6])     # 진료과



        self.attr2 = wx.grid.GridCellAttr()
        self.attr2.SetReadOnly()
        self.grid1.SetColAttr(1,self.attr2)  # 읽기전용으로 속성 변환.
        self.grid1.SetColAttr(2,self.attr2)
        self.grid1.SetColAttr(3,self.attr2)
        self.grid1.SetColAttr(4,self.attr2)
        self.grid1.SetColAttr(5,self.attr2)
        self.grid1.SetColAttr(6,self.attr2)

        self.grid1.AutoSizeColumn(2)

        self.sortflag = [-2,False]  # grid1 정렬위한 flag, 초기화 ,,,  self.sortflag[0] --> column 번호, self.sortflag[1] --> 역순정렬여부



    def gridPrepRow(self, grid, NewRowCount):
        # # NewRowCount 만큼  grid row 를 줄이거나 늘려라!! (첫칸부터 추가, 삭제)
        CurrentRowCount = grid.GetNumberRows()   # 현재 row count

        if CurrentRowCount > NewRowCount:
            grid.DeleteRows(0,CurrentRowCount - NewRowCount)  # 차이만큼 Row 삭제
        elif CurrentRowCount < NewRowCount:
            grid.InsertRows(0,NewRowCount - CurrentRowCount)   # 차이만큼 Row 삽입 하여  record count 만큼 row count 증가시킨다.


    def gridPrepCol(self, grid, newColCount):
        # # NewColCount 만큼  grid column 을  줄이거나 늘려라!! (첫칸부터 추가, 삭제)

        currentColCount = grid.GetNumberCols()


        if currentColCount > newColCount:
            grid.DeleteCols(0,currentColCount - newColCount )   # 차이만큼 column 삭제

        elif currentColCount < newColCount:
            grid.InsertCols(0,newColCount - currentColCount)




    def MyListRange( self, event ):
        pass

    def OnBtnAllSel( self, event ): # 전체 선택
        if (self.grid1.GetNumberRows()):  # grid1 에 data가 있으면  다음을 실행.
            for i in range(self.grid1.GetNumberRows()):  # grid1 의 row No. 0 부터 마지막 row No. 까지
                self.grid1.SetCellValue(i,0,'1')   # 모든 row 를 check 하기 !!
        for rec in self.grid1List3:
            print rec

    def OnBtnAllCan( self, event ):  # 전체 취소
        if (self.grid1.GetNumberRows()):  # grid1 에 data가 있으면  다음을 실행.
            for i in range(self.grid1.GetNumberRows()):  # grid1 의 row No. 0 부터 마지막 row No. 까지
                self.grid1.SetCellValue(i,0,'')   # 모든 row 를 check 해제 하기 !!


    def OnBtnRev( self, event ):  # 반전.
        if (self.grid1.GetNumberRows()):  # grid1 에 data가 있으면  다음을 실행.
            for i in range(self.grid1.GetNumberRows()):  # grid1 의 row No. 0 부터 마지막 row No. 까지
                if(self.grid1.GetCellValue(i,0)) :  # check 되어 있으면
                    self.grid1.SetCellValue(i,0,'')   # check 해제 하기 !!
                else:  self.grid1.SetCellValue(i,0,'1')     # check 해제 되어있으면 , check 하기...


    def OnGridLtClick( self, event ):  # grid1   left click

        if event.Col == 0:
            wx.CallLater(100,self.toggleCheckBox)

        event.Skip()




    def OnGridLtDClick( self, event ):

        grid1row = event.GetRow()  # double click 된 grid1 row No.

        ocmnum = self.grid1.GetCellValue(grid1row,5)   #  내원번호
        self.SendList(ocmnum, grid1row)

        event.Skip()


    def onEditorCreated( self, event ):          # self.grid1 을 시작할때. self.grid1.EnableEditing( True ) 로 되어있어야 한다!!!!  *****
        if event.Col == 0:
            self.cb = event.Control
            self.cb.WindowStyle |= wx.WANTS_CHARS
            self.cb.Bind(wx.EVT_KEY_DOWN,self.onGridKeyDown)
            self.cb.Bind(wx.EVT_CHECKBOX, self.onCheckBoxPrint)   # CheckBox 객체의 event 연결.
        event.Skip()

    def onCheckBoxPrint(self,event):
        self.myCheck(event.IsChecked())

    def OnG1LabelClick( self, event ):  # grid1 label click  for 정렬
        row = event.GetRow()
        col = event.GetCol()


        if (col == self.sortflag[0]): self.sortflag[1] = not self.sortflag[1]   # self.sortflag[1]  :  정렬 reverse 옵션 ---  True or  False
        else:
            self.sortflag[0] = col     # 정렬할 column
            self.sortflag[1] = False


        if ((col >=0) and(self.grid1.GetNumberRows()>0)):
            self.grid1List3 = sorted(self.grid1List3, key = lambda rec:rec[0][col], reverse=self.sortflag[1])  # 클릭한 label 순서로 정렬하여 self.grid1List3에 저장


            for i, rec in enumerate(self.grid1List3):            # grid1 에  데이터 표시하기!!!!
                self.grid1.SetCellValue(i,0,rec[0][0])   # check 표시
                self.grid1.SetCellValue(i,1,rec[0][1][:8])  # 처방일만 표시  ---   'YYYYMMDDHHMM' 중 앞 8자만 표시..
                self.grid1.SetCellValue(i,2,rec[0][2])
                self.grid1.SetCellValue(i,3,rec[0][3])
                self.grid1.SetCellValue(i,4,rec[0][4])
                self.grid1.SetCellValue(i,5,rec[0][5])     #  rec[4] --> ocmnum 내원번호
                self.grid1.SetCellValue(i,6,rec[0][6])     # 진료과



    def onCellSelected( self, event ):
        if event.Col == 0:
            wx.CallAfter(self.grid1.EnableCellEditControl)
        event.Skip()



    def OnBtnSend( self, event ):    # 선택환자 검사하기 버튼
        if (self.grid1.GetNumberRows()):  # grid1 에 data가 있으면  다음을 실행.
            for i in range(self.grid1.GetNumberRows()):  # grid1 의 row No. 0 부터 마지막 row No. 까지 ocmnum 를 찾아서 해당 cbc 항목, 결과 전송하라.
                if (self.grid1.GetCellValue(i,0)):   # check 되어있는 항목만 선택하여 보내라!!
                    ocmnum = self.grid1.GetCellValue(i,5)
                    grid1row = i
                    self.SendList(ocmnum, grid1row)


    def OnBtnCancelGrid2( self, event ):  # 선택취소 버튼, grid2
#        print self.grid2.GetGridCursorRow()   # click 된 grid2 row 값 반환 함수.
        if(self.grid2.GetNumberRows()):
            if(self.grid2.GetSelectionBlockTopLeft()):  # drag로 범위 설정시
                TLt = self.grid2.GetSelectionBlockTopLeft()
                BRt = self.grid2.GetSelectionBlockBottomRight()
                self.grid2.DeleteRows( TLt[0][0] , BRt[0][0]-TLt[0][0]+1)   # grid2 화면에서도 지우고
                del self.grid2List[TLt[0][0]:BRt[0][0]+1]   # grid2 와 연동된 self.grid2List 에서도 해당부분 삭제함.

            elif(self.grid2.GetSelectedCells()):   # Ctrl+click 으로 여러 cell 선택시
                selcells =self.grid2.GetSelectedCells()   # 선택순서대로 좌표값입력..
                selcells.sort(reverse=True)    # # 뒤집어서 큰 좌표부터 정렬  ---> 삭제시 문제 안생김.
                for i in selcells:
                    self.grid2.DeleteRows(i[0],1)   # i[0]  --->  선택된 row     # grid2 화면에서도 지우고
                    del self.grid2List[i[0]]             # grid2 와 연동된 self.grid2List 에서도 해당부분 삭제함.
            else:
                DelRow = self.grid2.GetGridCursorRow()
                self.grid2.DeleteRows(DelRow,1)   # 한개의 cell에 click 된 경우.    # grid2 화면에서도 지우고
                del self.grid2List[DelRow]             # grid2 와 연동된 self.grid2List 에서도 해당부분 삭제함.



    def OnBtnCancelAllGrid2( self, event ):    # 전체 취소 , grid2
        if(self.grid2.GetNumberRows()):
            self.grid2.DeleteRows(0,self.grid2.GetNumberRows())   # grid2 화면에서도 지우고
        self.grid2List =[]    # grid2 와 연동된 self.grid2List 에서도 삭제함.


    def onBtnSendData( self, event ):  # 양방향 전송시에만 사용하는 버튼,  환자및 검사 order 를  검사장비로 보낸다., labmachine => 검사장비instance
        self.labmach.makeSendData()




    def OnGrid2LtClick( self, event ):
        self.grid3.SetCellValue(0,1,self.grid2.GetCellValue(event.GetRow(),0) )   # 처방일 복사하기.
        self.grid3.SetCellValue(1,1,self.grid2.GetCellValue(event.GetRow(),1) )    # 검사일시 복사

#        print 'col no ==',self.grid2.GetNumberCols()  # self.grid2 의 column 갯수 구하기.
        labdatacolumnNo = self.grid2.GetNumberCols() - 7  # 7 == 검사결과 항목 column 이외의 환자정보 column

        # 추후에는 sqlite db 에 저장한 값을 가져와서 grid3에 출력할것 ( 이전 검사 포함하여)****
        for i in range(labdatacolumnNo):
            self.grid3.SetCellValue(i+2,1,self.grid2.GetCellValue(event.GetRow(),i+7) )

        event.Skip()



    def OnBtnSave( self, event ):   # 서버 MSSQL db 에 저장하기.
        # print ' --->    저장 버튼   눌림................'
        # print '        self.grid2List', self.grid2List
        sql = """update  ResInf
                 set ResRltVal = '%s'
                 where resocmnum = '%s'  and ResOdrSeq = '%s' """


        for i,data in enumerate(self.grid2List):   # grid2List 에 있는 모든 환자 자료 저장 ( 검사시행여부 상관없이)
#            print i, 'ocmnum =', data[0][6], 'exam date =', data[0][1], 'data =', data
            for k, lab in enumerate(data):
                if(k !=0):  # 환자정보 부분은 제외한다.
                    # print lab
                    if (lab[2] != None):  # lab 결과값이 None 아니면 저장하라!
                        updsql = sql %( lab[2], data[0][6],lab[1] )
                        # print 'updsql =', updsql
                        self.cursor.execute(updsql)



    # 수정요함 ,  elecrto 일때는 from electro....
    def Menudbview( self, event ):  # 메뉴 ; 보기 - 저장결과보기 (local sqlite3 db 에 저장된 결과보기)

        self.dbview = serialadd1.MyFrame2(None)
        self.dbview.Show()

        self.sqcur.execute("""select odrdate , examdate , chtno , name ,
                sampleno , dept , ocmnum ,
                wbc , rbc , hb , hct , mcv , mch , mchc ,
                plt , neutro , lymph , mono ,
                pdw , rdw , pct , mpv,rawdata  from cbc""")
        recs = self.sqcur.fetchall()

        rowcount  = len(recs)

        self.gridPrepRow(self.dbview.grid21, rowcount)

        for i,rec in enumerate(recs):
            for j,value in enumerate(rec):
#                print i, j, value
                if( value == None): value ='<null>'    # 이 줄 없으면, TypeError: coercing to Unicode: need string or buffer, NoneType found 발생!!!!
                val = unicode(value,'utf-8').encode('euc-kr')    #  utf-8로 인코딩된 value를 euc-kr 인코딩으로 변환
                self.dbview.grid21.SetCellValue(i,j,val)


    def RS232TextView( self, event ):  # 메뉴 ; 보기 - 전송자료모두보기
        self.txtviewer = RS232Viwer(None)
        self.RS232Viwerflag  = True
        self.txtviewer.Show()



    def toggleCheckBox(self):
        self.cb.Value = not self.cb.Value
        self.myCheck(self.cb.Value)


    def myCheck(self,IsChecked):  # self.grid1List3 정보 수정!!

        if(IsChecked):
            self.grid1List3[self.grid1.GridCursorRow][0][0] ='1'
        else: self.grid1List3[self.grid1.GridCursorRow][0][0] =''


    def onGridKeyDown(self,evt):
        if evt.KeyCode == wx.WXK_UP:
            if self.grid1.GridCursorRow > 0:
                self.grid1.DisableCellEditControl()
                self.grid1.MoveCursorUp(False)
        elif evt.KeyCode == wx.WXK_DOWN:
            if self.grid1.GridCursorRow < (self.grid1.NumberRows-1):
                self.grid1.DisableCellEditControl()
                self.grid1.MoveCursorDown(False)
        elif evt.KeyCode == wx.WXK_LEFT:
            if self.grid1.GridCursorCol > 0:
                self.grid1.DisableCellEditControl()
                self.grid1.MoveCursorLeft(False)
        elif evt.KeyCode == wx.WXK_RIGHT:
            if self.grid1.GridCursorCol < (self.grid1.NumberCols-1):
                self.grid1.DisableCellEditControl()
                self.grid1.MoveCursorRight(False)
        else:
            evt.Skip()



    def MyStart(self):
        # print 'start!!'
        self.attr = wx.grid.GridCellAttr()
        self.attr.SetEditor(wx.grid.GridCellBoolEditor())
        self.attr.SetRenderer(wx.grid.GridCellBoolRenderer())
        self.grid1.SetColAttr(0,self.attr)
        self.grid1.SetColSize(0,20)
        # print 'start!!2'
        # OCS db 서버 열기 (bit 차트)
        try:
            print '---'
            self.conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.10.200;DATABASE=DrBITPACK;UID=sa;PWD=bit', autocommit=True )
            print '---2'
        except Exception:
            mylog.msgLog('BIT server 연결  error' )
            return
        print 'start!!3'
        # print self.conn
        if self.conn:
            self.cursor = self.conn.cursor()
        else:
            print self.conn

        # print 'start!!4'

        self.grid1List3 =[]  # grid1 에 표시할 실제 데이터 row로 구성된 list
                                # self.grid1List3[0] = 환자정보,           [1],[2],.... = (cbc labcode, order seq, value) 튜플
                                # [0][0] = 체크여부, [0][1] = 오더일, [0][2]=차트번호,  [0][3]= 이름,  [0][4] = 진행상황, [0][5] = ocmnum,  [0][6] = dept

        self.grid2List=[]   # grid2 에 표시할 실제 데이터 row로 구성된 list
                                # self.grid2List[0] = 환자정보,           [1],[2], .... = [cbc labcode, order seq, value], [cbc labcode, order seq, value],....
                                # [0][0] = 처방일, [0][1] = 검사일시, [0][2]=차트번호,  [0][3]= 이름,  [0][4] = 검체번호, [0][5] =dept ,  [0][6] =  ocmnum

        # 장비에 따른 검사항목정보 알기위해  sqlite db 연결하기
        self.sqconn = sqlite3.connect('lab.db',isolation_level=None) # isolation_level=None ; autocommit
        self.sqcur = self.sqconn.cursor()
        self.sqconn.text_factory = str  # unicode error 해결!!!
        # print 'start!!3'
        sql = "select * from machine  where grp = '%s' and sel ='Y'"      # sel ='Y' ===> default 로 선택된 장비 , grp ==> 장비종류별그룹(cbc,elctro,chem...)  정보가져오기!!!
        self.sqcur.execute(sql%self.labmode)
        data = self.sqcur.fetchall()

        machine = data[0][1]      # 장비명   ex) LC-550, KX-21N ....
        comport = data[0][4]
        baudrate = data[0][5]
        bytesize = data[0][6]
        parity = data[0][7]
        stopbit = data[0][8]
        timeout = float(data[0][9])   # 문자 '0.5' 를  float 로 형변환해야 한다!!!

        # print 'machine == ', machine

        sql = """
                    select b.ocscode, a.name
                        from  labcode a  inner join codemachmatch b
                        on a.ocscode = b.ocscode
                        where b.machine='%s'
                    """
        self.sqcur.execute(sql%machine)    # 장비에서 검사가능한 검사항목(ocscode, labname)정보 가져오기
        self.labcodedata = self.sqcur.fetchall()
        if(len(self.labcodedata)==0):
            mylog.msgLog('no data' )
        # print 'self.labcodedata ==', len(self.labcodedata), self.labcodedata



        self.gridPrepCol(self.grid2,len(self.labcodedata)+7)  # 검사항목에 따른 grid2 column 길이 조절  # "처방일", "검사일시", "차트번호", "이름" ,  "검체번호",  "진료과", '내원번호'  항목때문에 7을 더한다.
        self.grid2SetColLabel(self.labcodedata)   # 검사이름을 grid2   column label 에 먼저 표시한다.

        self.gridPrepRow(self.grid3,len(self.labcodedata)+2)   # 검사항목에 따른 grid3 row 길이 조절 - '처방일', '검사일시' 줄때문에 2를 더한다!
        self.grid3SetLabel(self.labcodedata)  # 검사이름을 grid3 에 먼저 표시한다.
        self.grid3.SetDefaultCellAlignment(wx.ALIGN_CENTRE,wx.ALIGN_CENTRE)    # grid3 가운데 정렬!!


        self.sortflag = [-2,False]  # grid1 정렬위한 flag, 초기화     ;  [0] = column number,   [1] = reverse 상태(True, False)


        self.th1 = threading.Thread(target=self.onRS232Thread,args=(comport,baudrate,bytesize,parity,stopbit,timeout,machine))
#        print 'thread made'
        self.th1evt = threading.Event()
        self.th1.setDaemon(1)
        self.th1evt.set()
        self.th1.start()



    def grid2SetColLabel(self,labelList):      # grid2   검사항목  column label 지역에  표시하기

        colNo =7

        for ocscode,name in labelList:
            self.grid2.SetColLabelValue( colNo,name )
            colNo +=1

        self.grid2.SetColLabelValue( 0, u"처방일" )
        self.grid2.SetColLabelValue( 1, u"검사일시" )
        self.grid2.SetColLabelValue( 2, u"차트번호" )
        self.grid2.SetColLabelValue( 3, u"이름" )
        self.grid2.SetColLabelValue( 4, u"검체번호" )
        self.grid2.SetColLabelValue( 5, u"진료과" )
        self.grid2.SetColLabelValue( 6, u"내원번호" )



    def grid3SetLabel(self, labelList):   # grid3   검사항목  표시하기
        self.grid3.SetCellValue(0,0,'처방일')
        self.grid3.SetCellValue(1,0, '검사일시')

        rowNo =2   # 처방일, 검사일시 항목 2줄  다음 부터.. labelList 입력하기 위함.

        for ocscode,name in labelList:
            self.grid3.SetCellValue(rowNo,0,name)
            rowNo += 1



    def SendList(self, ocmnum, grid1row):  # grid1 row 에 해당하는 ocmnum 의 검사항목, 결과값을 grid2 로 보낸다!

        ptinfo=[]  # grid2List[0] 에 사용할  환자정보 임시보관 list
                        # [0] = 처방일, [1] = 검사일시, [2]=차트번호,  [3]= 이름,  [4] = 검체번호, [5] =dept , [6] =  ocmnum

        rectmp =[]   #  grid2 환자 record  정보 , rectmp == [[환자정보],['ocscode',orderline,검사결과값],['ocscode',orderline,검사결과값],...]

#        print 'self.grid1List3[grid1row] =', self.grid1List3[grid1row]
        ptinfo.append(self.grid1List3[grid1row][0][1])    # [0] = 처방일
        ptinfo.append('')                                     # [1] = 검사일시
        ptinfo.append( self.grid1List3[grid1row][0][2])   # [2]=차트번호
        ptinfo.append( self.grid1List3[grid1row][0][3])   # [3]= 이름
        ptinfo.append('')                                      #  [4] = 검체번호
        ptinfo.append(self.grid1List3[grid1row][0][6])   #  [5] =dept
        ptinfo.append(self.grid1List3[grid1row][0][5] )   # [6] =  ocmnum
#        print 'ptinfo =', ptinfo

        if(len(self.grid2List) != 0) :   # 데이터 있으면 , 중복인지 검사!!
            for data in self.grid2List:
                if(data[0][6] == ocmnum):
                    if(data[0][5] ==self.grid1List3[grid1row][0][6]):   # dept 가 같은가?
                        if(data[1][1] ==self.grid1List3[grid1row][1][1] ):  # 처음 labcode 의 order seq 중복인가
                            return


        rectmp.append(ptinfo)

        for i, data in enumerate(self.grid1List3[grid1row]):  # 선택환자의 labcode , order seq, value  정보를 가져와서 rectmp 에 추가한다.
#            print i, data
            if(i != 0) :
                rectmp.append(list(data))   # 검사결과값 입력위해 list 로 형변환.
#                print data[0]

#        print  'rectmp =',   rectmp
#        print ' length of rectmp =', len(rectmp)

        self.grid2List.append(rectmp)  # 선택한 환자를 self.grid2List 에 넣는다.

#        print 'self.grid2List =', self.grid2List
#        print ' length of self.grid2List =', len(self.grid2List)
#        print 'self.grid1List3 =', self.grid1List3
#        print ' length of self.grid2List =', len(self.grid1List3)

        self.grid2.AppendRows(1)

        AddedRow = self.grid2.GetNumberRows() -1

        self.grid2.SetCellValue(AddedRow,0,self.grid2List[AddedRow][0][0])  # 처방일

        self.grid2.SetCellValue(AddedRow,2,self.grid2List[AddedRow][0][2])  # 차트번호
        self.grid2.SetCellValue(AddedRow,3,self.grid2List[AddedRow][0][3])  # 이름

        self.grid2.SetCellValue(AddedRow,5,self.grid2List[AddedRow][0][5])  # 진료과
        self.grid2.SetCellValue(AddedRow,6,self.grid2List[AddedRow][0][6])  # 내원번호

        # rectmp == [[환자정보],['ocscode',orderline,검사결과값],['ocscode',orderline,검사결과값],...]
        for i, data in enumerate(rectmp):   #검사결과 gird2 에 출력, None 이면 'V' 로 표시..
            if(i != 0) :  # rectmp[0] - 환자 정보이므로 skip!
                for k, code in enumerate(self.labcodedata):    # self.labcodedata ==  [('B1050', 'WBC'), ('B1040', 'RBC'), ('B1010', 'Hb'),...]
#                    print 'k, code, name =', k, code
                    if(data[0].strip() == code[0]):    # data[0] == ocscode
                        if(data[2]) :   # data[2] == 검사결과값
                            self.grid2.SetCellValue(AddedRow, k+7, data[2])
                        else:
                            self.grid2.SetCellValue(AddedRow, k+7, 'V')    # 결과값이 없으면 grid2 cell 에   'V' 를 입력하라.


        print 'self.grid2List ==>', self.grid2List


    def onRS232Thread(self,comport,baudrate,bytesize,parity,stopbit,timeout,machineName):
        # print 'start'
        # print 'port ' , comport
        # print  'baurate =', baudrate,bytesize,parity,stopbit,timeout

        try :
            self.ser = serial.Serial()    #com1 port 연결, Open port 0 at “9600,8,N,1”, no timeout:
            self.ser.port = comport
            self.ser.baudrate = baudrate
            self.ser.bytesize = bytesize
            self.ser.parity = parity
            self.ser.stopbits = stopbit
            self.ser.timeout= timeout   # timeout 설정  = 0.5초

            self.ser.open()

            self.ser.flushInput()  # input buffer 쓰레기값 있으면 비우기..
            self.ser.flushOutput()  # output buffer 쓰레기값 있으면 비우기..
#            print ' open port!'




#            if(self.ser.isOpen()):
#                print ' ++++++++++++++   com port  open!!!'
#            else:
#                print ' ---------------- cannot open com port!!!'

        except :
            # print ' COM 포트 열수 없습니다.'
            mylog.msgLog( ' COM 포트 열수 없습니다.' )
            time.sleep(1)
            self.Destroy()
            # print ' expire============ '
            mylog.msgLog(  'expire============ '   )
            return


#        cbc = ''  # cbc buffer

        self.thconn = sqlite3.connect('lab.db',isolation_level=None) # isolation_level=None ; autocommit
        self.thcur = self.thconn.cursor()
        self.thconn.text_factory = str  # unicode error 해결!!!

        if (self.labmode == 'cbc'):
            # print ' self.labmode  === cbc'
            # print ' machine name =====' , machineName
            if(machineName == 'LC-550'):
                self.labmach = CBCLC550(self.grid2,self)  # 검사장비 instance 생성.
            elif (machineName == 'KX-21N'):
                self.labmach = CBCKX21N(self.grid2,self)  # 검사장비 instance 생성.
            else:
                wx.MessageBox("해당 CBC 장비가 없습니다.", caption="Notice", style=wx.OK)
                return
        elif(self.labmode == 'electro'):
            # print ' self.labmode  === electro ================'
            self.labmach = Electo9180(self.grid2,self)  # 검사장비 instance 생성.
        elif(self.labmode == 'chem'):
            # print ' self.labmode  === chem 장비  ================'
            # print ' machine name =====' , machineName
            if(machineName == 'Prestige24i'):
                # print ' prestige ok'
                self.labmach = Prestige24i(self.grid2,self ) # 검사장비 instance 생성.
                self.btnSendData.Enable(True)  # labdesign module 의  MyForm class의 self.btnSendData widget을 enable 시킨다.



        while(self.th1evt.isSet()):
            labdata = self.ser.read(1)              # thread를 daemon으로 하지않으면  값 들어올때 까지 프로그램 정지!!!!
                                                    # timeout 시간동안에  시리얼버퍼에 읽을 데이터 없으면  labdata =''  반환...
            self.labmach.serialData = self.labmach.serialData + labdata

            self.labmach.getData(self) # getData 인수로 현재 class 인스턴스(self)를 넘긴다..



        if(self.ser.isOpen()):
            self.ser.close()    # close port

#        print 'End RS232 thread================'

        self.thconn.close()   # close thread sqlite db




    def Sql(self,sql):
        self.thcur.execute(sql)


    def MakeDb(self): # 최초 설치시 cbc.db 만들기
        conn = sqlite3.connect('lab.db' ,isolation_level=None)  #autocommit
        c= conn.cursor()

        # cbc table 만들기
        c.execute(''' create table cbc
                        (odrdate text, examdate text, chtno text, name text,
                        sampleno text, dept text, ocmnum text,
                        wbc text, rbc text, hb text, hct text, mcv text, mch text, mchc text,
                        plt text, neutro text, lymph text, mono text,
                        pdw text, rdw text, pct text, mpv text, rawdata BLOB )'''  )    #  dept ; 진료과

        # ocs server 접속 table 만들기
        c.execute( """
            CREATE TABLE OCSserver (
              serverdb TEXT,
              serverip TEXT,
              dbname TEXT,
              uid TEXT,
              pwd TEXT);  """ )

        #  sampleno  --> 검체번호, examno  ---> 검사차수
        conn.close()
        pass


class CBCKX21N():   # Sysmex KX-21N 장비 제어
    def __init__(self,grid,inst):
        self.name = 'KX-21N'   # 검사장비명.

        self.serialData = ''    # RS232C 에서 넘어온 raw data 저장.
        self.rowcnt = 0     # self.grid2 의  row count 저장!
        self.row = 0        # '검사일시' 비어있는 처음 발견되는 row 가 결과 입력할 row 이다..!!
        self.grid = grid    # grid  ; 결과 출력한 grid
        self.inst = inst
#        self.inst.grid2List    # grid2 검사결과 저장 list  --> grid2 에 표시하고, 이 list에 저장한다.

        # print 'inst.grid2List =', self.inst.grid2List

    def getData(self,inst):  # instance를 인수 전달.

        if(len(self.serialData)>100):
##                self.rowcnt = self.grid2.GetNumberRows()
            self.setrowcnt(self.grid)   # self.rowcnt setting
            if((self.serialData[0]=='\x02')and (self.serialData[-1] =='\x03')):
                labtime = str(datetime.datetime.now())  # 문자열로 바꾸어 timestamp 저장. (검사일시)
                self.row = -1  # 검사결과 입력될 row 초기화
                self.findEmptyRow(self.grid)  # self.row setting     #입력받은 grid 에서 비어있는 row 찾기...

                # print 'serial data ==', self.serialData

                if (self.row != -1):   # 결과입력 할 row 가 있으면

                    sampleNo = self.serialData[11:23] # sample ID No.  12자리

                    wbc = self.serialData[30:33] + '.' + self.serialData[33]  # wbc  --> float type으로 변환해야함.
                    rbc = self.serialData[35:37] + '.' + self.serialData[37:39] # rbc
                    hb = self.serialData[40:43] + '.' + self.serialData[43]  # Hb
                    hct = self.serialData[45:48] + '.' + self.serialData[48]  # Hct
                    mcv = self.serialData[50:53] + '.' + self.serialData[53]  # MCV
                    mch = self.serialData[55:58] + '.' + self.serialData[58]  # MCH
                    mchc = self.serialData[60:63] + '.' + self.serialData[63]  # MCHC
                    plt = self.serialData[65:69]  # Platelet   --> int type 으로 변환해야함.

                    lym = self.serialData[70:73] + '.' + self.serialData[73]  # lymphocyte%
                    mxd = self.serialData[75:78] + '.' + self.serialData[78]  # mix% (monocyte%)
                    neut = self.serialData[80:83] + '.' + self.serialData[83]  # neutrophil%

                    rdw = self.serialData[100:103] + '.' + self.serialData[103]  # RDW
                    pdw = self.serialData[105:108] + '.' + self.serialData[108]  # PDW
                    mpv = self.serialData[110:113] + '.' + self.serialData[113]  # MPV

                    # str type 을 float type 문자열로 변환 ( ex: '012.4' --> '12.4' 로 변환)
                    cbcdata = self.typeConvertFloat([wbc,rbc,hb,hct,mcv,mch,mchc,lym,mxd,neut,rdw,pdw,mpv])
                    wbc,rbc,hb,hct,mcv,mch,mchc, lym,mxd,neut, rdw,pdw,mpv = cbcdata
                    plt = self.typeConvertInt(plt)


                    self.grid.SetCellValue(self.row,4,sampleNo)  # 검체번호

                    self.Grid2Value(self.row,7, wbc, 'B1050')   # wbc
                    self.Grid2Value(self.row,8, rbc, 'B1040')  # rbc
                    self.Grid2Value(self.row,9, hb, 'B1010')  # Hb
                    self.Grid2Value(self.row,10, hct, 'B1020') # Hct
                    self.Grid2Value(self.row,11, mcv, 'B1020A')  #MCV
                    self.Grid2Value(self.row,12, mch, 'B1020B')  # MCH
                    self.Grid2Value(self.row,13, mchc, 'B1020C') # MCHC
                    self.Grid2Value(self.row,14, plt, 'B1060')  # Platelet

                    self.Grid2Value(self.row,20, mpv, 'B1060B') # MPV
                    self.Grid2Value(self.row,18, rdw, 'B1220') # RDW
                    self.Grid2Value(self.row,19, pdw, 'B1230') # PDW

                    self.Grid2Value(self.row,15, neut, 'B1091A') # neutrophil%
                    self.Grid2Value(self.row,16, lym, 'B1091B')  # lymphocyte%
                    self.Grid2Value(self.row,17, mxd, 'B1091C') # mix% (monocyte%)


                    # grid2 에 timestamp 검사시간 저장 ...
                    self.grid.SetCellValue(self.row,1,labtime[:-7])  # ????    grid2List 에는 저장안함

                    #-------  RS232 data 를 sqlte db 에 저장하기. -----
                    tmprec=[]
#                    # for col in range(self.grid.GetNumberCols()) :
##                            print self.grid2.GetCellValue(self.row,col)
                    for col in range(7) : # 환자 기본 정보만 불러오기
                        tmprec.append(self.grid.GetCellValue(self.row,col) )

                    for data in cbcdata:
                        tmprec.append(data)

                    tmprec.insert(14,plt)

                    cbchex = self.serialData.encode('hex')
                    tmprec.append(cbchex)
                    # print 'tmprec == ', tmprec
                    sql = '''insert into cbc (odrdate , examdate , chtno , name , sampleno ,
                            dept , ocmnum , wbc , rbc , hb ,
                            hct , mcv , mch , mchc , plt ,
                            lymph , mono , neutro, rdw , pdw ,
                            mpv,rawdata )
                            values (?,?,?,?,?,  ?,?,?,?,?,  ?,?,?,?,?,  ?,?,?,?,?,  ?,?) '''   # 로컬 sqlite3에 저장한다.

                    inst.thcur.execute(sql,tmprec)  # getData 인수로 전달된  instance 사용한다.

#                if(self.RS232Viwerflag):   #메뉴 - '전송자료모두보기' 에 자료 보내기
##                        print 'self.RS232Viwerflag =', self.RS232Viwerflag
#                    data = self.serialData.decode('ascii','ignore')         # UnicodeDecodeError  에러 무시하기 위해 'ignore' 옵션 적용!!!!
#                    self.txtviewer.RS232TxtCtrl.AppendText(data)
#                    self.txtviewer.RS232TxtCtrl.AppendText('============================== END =========================\n')

                self.serialData =''   # self.serialData buffer 초기화

                # print 'inst.grid2List  test!!  ==', self.inst.grid2List


    def typeConvertFloat(self,datalist):  # str type 인 list변수를 float 로 변환하여  다시 str type list로 반환
        vallist = []
        for data in datalist:
            try:
                vallist.append(str(float(data)))
            except:
                vallist.append(data)
        return vallist

    def typeConvertInt(self,val):  # str type 인 val 변수를  int type 으로 변환하여  다시 str type으로 반환 (platelet 만 적용됨)
        try:
            val = str(int(val))
        except:
            val = val
        return val

    def setrowcnt(self,grid):
        self.rowcnt = grid.GetNumberRows()



    def findEmptyRow(self,grid):
        if(self.rowcnt):
            for i in range(self.rowcnt):
                if (grid.GetCellValue(i,1)==''):  # '검사일시' 비어있는 cell 을 찾아서, 처음 발견되는 row 가 결과 입력할 row 이다..!!!!
                    self.row = i
                    break

    def Grid2Value(self,row,col,value,labcode):        # 검사결과를 grid2 화면에 표시하고, self.grid2List 에도 결과값 입력한다.
        wx.CallAfter(self.grid.SetCellValue,row,col,value )
#        self.grid.SetCellValue(row,col,value)
        for i, labdata in enumerate(self.inst.grid2List[row]):   # self.grid2List 해당 row 에서  labcode 있으면 결과값 입력한다.
            if(i != 0):
                if(labdata[0].strip() == labcode ):    # labcode 있으면 결과값 입력한다.
                    labdata[2] = value




class CBCLC550():   # Horiba LC-550 장비 제어
    def __init__(self,grid,inst):
        self.name = 'LC-550'   # 검사장비명.

        self.serialData = ''    # RS232C 에서 넘어온 raw data 저장.
        self.rowcnt = 0     # self.grid2 의  row count 저장!
        self.row = 0        # '검사일시' 비어있는 처음 발견되는 row 가 결과 입력할 row 이다..!!
        self.grid = grid    # grid  ; 결과 출력한 grid
        self.inst = inst
#        self.inst.grid2List    # grid2 검사결과 저장 list  --> grid2 에 표시하고, 이 list에 저장한다.

        print 'inst.grid2List =', self.inst.grid2List

    def getData(self,inst):  # instance를 인수 전달.

        if(len(self.serialData)>100):
##                self.rowcnt = self.grid2.GetNumberRows()
            self.setrowcnt(self.grid)   # self.rowcnt setting
            if((self.serialData[0]=='\x02')and (self.serialData[-1] =='\x03')):
                labtime = str(datetime.datetime.now())  # 문자열로 바꾸어 timestamp 저장. (검사일시)
                self.row = -1  # 검사결과 입력될 row 초기화
                self.findEmptyRow(self.grid)  # self.row setting     #입력받은 grid 에서 비어있는 row 찾기...

##                    if(self.rowcnt):
##                        for i in range(self.rowcnt):
##                            if (self.grid2.GetCellValue(i,1)==''):  # '검사일시' 비어있는 cell 을 찾아서, 처음 발견되는 row 가 결과 입력할 row 이다..!!!!
##                                row = i
##                                break
#                        else:
#                            print 'no detected empty row !!!!!'
#                    print ' -=================='
#                    if (self.serialData[0]=='\x02'): print 'STX'
#                    if (self.serialData[-1] =='\x03'): print 'ETX'
#                    print '데이터 길이 =', int(self.serialData[1:6])
#                    print ' +++++++++++++++++++++++++++++'
##                    print self.serialData
#                    print ' ========================= '

                ttt= self.serialData.split('\r')

                if (self.row != -1):   # 결과입력 할 row 가 있으면
                    # print ttt
                    for i, data in enumerate(ttt):
                        # print 'line No = ',i, '  ; data =  ',data, ' === ',data[2:7]
                        if (i >= 9 and i <= 26):
                            try:
                                val = str(float(data[2:7]))
                            except:
                                val = data[2:7]
                                pass
                        if (i ==5):   # 검체번호 (1일 총 검사수 기준)
                            self.grid.SetCellValue(self.row,4,data[2:6])
                        if (i==9):
                            labcode = 'B1050'
                            self.Grid2Value(self.row,7, val, labcode)

                        if (i==10):
                            labcode = 'B1040'
                            self.Grid2Value(self.row,8, val, labcode)
                        if (i==11):
                            labcode = 'B1010'
                            self.Grid2Value(self.row,9, val, labcode)

                        if (i==12):
                            labcode = 'B1020'
                            self.Grid2Value(self.row,10, val, labcode)
                        if (i==13):
                            labcode = 'B1020A'
                            self.Grid2Value(self.row,11, val, labcode)
                        if (i==14):
                            labcode = 'B1020B'
                            self.Grid2Value(self.row,12, val, labcode)
                        if (i==15):
                            labcode = 'B1020C'
                            self.Grid2Value(self.row,13, val, labcode)
                        if (i==16):   #'  PDW'
                            labcode = 'B1230'
                            self.Grid2Value(self.row,19, val, labcode)
                        if (i==17):  # '  PLT'
                            labcode = 'B1060'
                            self.Grid2Value(self.row,14, val, labcode)
                        if (i==18):  # '  MPV'
                            labcode = 'B1060B'
                            self.Grid2Value(self.row,21, val, labcode)
                        if (i==19):  #'  PCT'
                            labcode = 'B1060A'
                            self.Grid2Value(self.row,20, val, labcode)
                        if (i==20):   #'  RDW'
                            labcode = 'B1220'
                            self.Grid2Value(self.row,18, val, labcode)
                        if (i==21):   # '  Lymphocyte'
                            labcode = 'B1091B'
                            self.Grid2Value(self.row,16, val, labcode)
                        if (i==22):    # '  monocyte'
                            labcode = 'B1091C'
                            self.Grid2Value(self.row,17, val, labcode)
                        if (i==23):    # '  granulocyte'
                            labcode = 'B1091A'
                            self.Grid2Value(self.row,15, val, labcode)
#                            if (i==24): print float(data[2:7]) , '  lym#'
#                            if (i==25): print float(data[2:7]) , '  mon#'
#                            if (i==26): print float(data[2:7]) , '  gran#'

                    # grid2 에 timestamp 검사시간 저장 ...
                    self.grid.SetCellValue(self.row,1,labtime[:-7])  # ????    grid2List 에는 저장안함

                    tmprec=[]

                    for col in range(self.grid.GetNumberCols()) :
#                            print self.grid2.GetCellValue(self.row,col)
                        tmprec.append(self.grid.GetCellValue(self.row,col) )

                    cbchex = self.serialData.encode('hex')
                    tmprec.append(cbchex)
                    sql = '''insert into cbc (odrdate , examdate , chtno , name ,
                            sampleno , dept , ocmnum ,
                            wbc , rbc , hb , hct , mcv , mch , mchc ,
                            plt , neutro , lymph , mono ,
                            rdw , pdw , pct , mpv,rawdata )
                            values (?,?,?,?,?,  ?,?,?,?,?,  ?,?,?,?,?,  ?,?,?,?,?,  ?,?,?) '''   # 로컬 sqlite3에 저장한다.

                    inst.thcur.execute(sql,tmprec)  # getData 인수로 전달된  instance 사용한다.



#                if(self.RS232Viwerflag):   #메뉴 - '전송자료모두보기' 에 자료 보내기
##                        print 'self.RS232Viwerflag =', self.RS232Viwerflag
#                    data = self.serialData.decode('ascii','ignore')         # UnicodeDecodeError  에러 무시하기 위해 'ignore' 옵션 적용!!!!
#                    self.txtviewer.RS232TxtCtrl.AppendText(data)
#                    self.txtviewer.RS232TxtCtrl.AppendText('============================== END =========================\n')

                self.serialData =''   # self.serialData buffer 초기화

                # print 'inst.grid2List  test!!  ==', self.inst.grid2List



    def setrowcnt(self,grid):
        self.rowcnt = grid.GetNumberRows()



    def findEmptyRow(self,grid):
        if(self.rowcnt):
            for i in range(self.rowcnt):
                if (grid.GetCellValue(i,1)==''):  # '검사일시' 비어있는 cell 을 찾아서, 처음 발견되는 row 가 결과 입력할 row 이다..!!!!
                    self.row = i
                    break

    def Grid2Value(self,row,col,value,labcode):        # 검사결과를 grid2 화면에 표시하고, self.grid2List 에도 결과값 입력한다.
        wx.CallAfter(self.grid.SetCellValue,row,col,value )
#        self.grid.SetCellValue(row,col,value)
        for i, labdata in enumerate(self.inst.grid2List[row]):   # self.grid2List 해당 row 에서  labcode 있으면 결과값 입력한다.
            if(i != 0):
                if(labdata[0].strip() == labcode ):    # labcode 있으면 결과값 입력한다.

                    labdata[2] = value




class Electo9180():   # grid  ; 결과 출력한 grid
    def __init__(self,grid, inst):
        self.name = '9180'   # 검사장비명.

        self.serialData = ''    # RS232C 에서 넘어온 raw data 저장.
        self.rowcnt = 0     # self.grid2 의  row count 저장!
        self.row = 0        # '검사일시' 비어있는 처음 발견되는 row 가 결과 입력할 row 이다..!!
        self.grid = grid    # grid  ; 결과 출력한 grid --> 여기서는 grid2
        self.inst = inst
#        self.grid2List = grid2List   # grid2 검사결과 저장 list  --> grid2 에 표시하고, 이 list에 저장한다.

        # print 'electro self.grid2List =', self.inst.grid2List

    def getData(self,inst):  # instance를 인수 전달.
        # print '==== getData mechod ====='
        if(len(self.serialData)>50):
            if((self.serialData[0]=='\x02') and (self.serialData[-1] =='\x03')):
                self.setrowcnt(self.grid)   # self.rowcnt setting
                labtime = str(datetime.datetime.now())  # 문자열로 바꾸어 timestamp 저장. (검사일시)
                self.row = -1  # 검사결과 입력될 row 초기화
                self.findEmptyRow(self.grid)  # self.row setting     #입력받은 grid 에서 비어있는 row 찾기...
                # print 'self.row 1 ===',self.row

                ttt= self.serialData.split('\r\n')
                # print 'electro ttt ====' , ttt

                if (self.row != -1):   # 결과입력 할 row 가 있으면
                    calMode = False   # calibration mode (True) 면, data 입력 안하고 skip 하기 위함.

                    for i, data in enumerate(ttt):
                        print 'i =',  i,'; data = ',data
                        if(i==9):    # 검체번호 입력
                            # print i, data,'----> ', data[10:],'len=', len(data[10:])
                            # print data[:9]  # 'Sample No'
                            # print 'self.row 2 ===',self.row

                            if((data[:9] == 'Sample No')):
                                self.grid.SetCellValue(self.row,4,data[10:].strip())
                            else:
                                # print i, data,'----> ', data[3:8]
                                calMode = True    # calibration mode 면  break !!!
                                break  # Calibration 시에는   Sample No 없으므로 중단한다!!!

                        if (i >= 12 and i <= 14):
                            try:
                                val = str(float(data[3:8]))
                            except:
                                val =''
                                pass
                            if (i==12):    # Na
                                labcode = 'C3791'
                                self.Grid2Value(self.row,7, val.split('.')[0], labcode)
                                # val.split('.')[0] --> '135.0' 를  '135'로 바꾼다.
                            if (i==13):    # K
                                labcode = 'C3792'
                                self.Grid2Value(self.row,8, val, labcode)
                            if (i==14):    # Cl
                                labcode = 'C3793'
                                self.Grid2Value(self.row,9, val.split('.')[0], labcode)

                    if(not calMode):  # calibraton mode 아니면 , 검사일시 기록
                        self.grid.SetCellValue(self.row,1,labtime[:-7])  # 검사일시 기록


                self.serialData =''   # self.serialData buffer 초기화

                # print ' electro   self.inst.grid2List  test!!  ==', self.inst.grid2List



    def setrowcnt(self,grid):
        self.rowcnt = grid.GetNumberRows()


    def findEmptyRow(self,grid):
        if(self.rowcnt):
            for i in range(self.rowcnt):
                if (grid.GetCellValue(i,1)==''):  # '검사일시' 비어있는 cell 을 찾아서, 처음 발견되는 row 가 결과 입력할 row 이다..!!!!
                    self.row = i
                    break

    def Grid2Value(self,row,col,value,labcode):        # 검사결과를 grid2 화면에 표시하고, self.grid2List 에도 결과값 입력한다.
        wx.CallAfter(self.grid.SetCellValue,row,col,value )
#        self.grid.SetCellValue(row,col,value)
        # print 'self.inst.grid2List[row] ==', self.inst.grid2List[row]
        for i, labdata in enumerate(self.inst.grid2List[row]):   # self.grid2List 해당 row 에서  labcode 있으면 결과값 입력한다.
            if(i != 0):
                if(labdata[0].strip() == labcode ):    # labcode 있으면 결과값 입력한다.
                    labdata[2] = value

        # print 'new self.inst.grid2List[row] =', self.inst.grid2List[row]




class Prestige24i:   # grid  ; 결과 출력한 grid

    # prescodedict ==>  key = prestige code, value =( ocscode, 소수점이하 표시 자릿수) 로 구성됨...
    prescodedict = {'CREA': ('C3750', 2), 'DBIL': ('C3721', 2), 'PHOS': ('C3794', 1), 'TG': ('C2443', 0), 'GGT': ('B2710', 0), 'UIBC': ('C2510', 0), 'CA': ('C3795', 1), 'LDH': ('B2590', 0), 'ALB': ('C2210', 2), 'TP': ('C2200', 2), 'HDLC': ('C2420', 0), 'TBIL': ('C3720', 2), 'BUN': ('C3730', 1), 'GOT': ('B2570', 0), 'GPT': ('B2580', 0), 'TCHOL': ('C2411', 0), 'UA': ('C3780', 2), 'GLU': ('C3711', 0), 'ALP': ('B2602', 0), 'FE': ('C2490', 0)}

    # grid2codematch  ==> key = ocscode, value =('검사명',grid2 column 번호) ; ocscode 에 해당하는 grid2 column 번호 구하여 결과값 입력하기위해 사용...
    grid2codematch = {'C3730PO': ('BUN-post', 27), 'C2420': ('HDL-chol', 24), 'B2710': ('r-GTP', 15), 'C3795': ('Ca', 20), 'C3794': ('P', 21), 'C2443': ('TG', 23), 'C3780': ('uric acid', 19), 'C2510': ('UIBC', 26), 'C2411': ('T-cholestero', 22), 'B2602': ('ALP', 16), 'B2570': ('GOT', 11), 'B2590': ('LDH', 18), 'C2210': ('albumin', 10), 'C2200': ('Protein', 9), 'C2490': ('Fe', 25), 'C3711': ('glucose', 17), 'C3750': ('Cr', 8), 'B2580': ('GPT', 12), 'C3730': ('BUN', 7), 'C3720': ('T-bilirubin', 13), 'C3721': ('d-bilirubin', 14)}

    # Prestige 내의 시약 item 순서 번호 사전 ( 시약 : 시약번호 )
    itempos = {'CREA': 15, 'GGT': 5, 'DBIL': 13, 'TBIL': 12, 'UIBC': 20, 'CA': 18, 'LDH': 3, 'ALB': 7, 'PHOS': 17, 'TP': 6, 'HDLC': 9, 'TG': 10, 'BUN': 14, 'FE': 19, 'TCHOL': 8, 'GPT': 2, 'GOT': 1, 'UA': 16, 'GLU': 11, 'ALP': 4}


    def __init__(self,grid, inst):
        self.name = 'Prestige24i'   # 검사장비명.
        self.sender = 'HOST^P_1'    # 데이터전송에 필요한  host PC name  --> Prestige 장비에서 설정된 이름.
        self.receiver  = 'Prestige24i^SYSTEM1'     # 데이터전송시 필요한  검사장비 name  --> Prestige 장비에서 설정된 이름.

        self.serialData = ''    # RS232C 에서 넘어온 raw data 저장.
        self.rowcnt = 0     # self.grid2 의  row count 저장!
        self.row = 0        # '검사일시' 비어있는 처음 발견되는 row 가 결과 입력할 row 이다..!!
        self.grid = grid    # grid  ; 결과 출력한 grid --> 여기서는 grid2
        self.inst = inst

        self.isSendBtnPressed = False   # 데이터 전송 버튼 누른경우!
        self.isTxState = 'No' # Tx mode 상태 표시 ,('No','Begin','Transfer' 3가지중 한가지 값을 가진다.)
                              # 'No' -> Rx mode, 'Begin' -> Tx establishment state(host Pc -> Prestige로 전송모드시작.)
                              # 'Transfer' -> Prestige로 전송중인 상태, 'End' -> Tx 종료모드

        self.countBeginNAK = 0      # 전송모드, establishment phase NAK count     같은 명령어 , 데이터 전송횟수  -- 6회면 EOT 보내고 tx mode 종료함.
        self.countTransferNAK = 0   # 전송모드, Transfer phase NAK count

        self.isNoResposeBegin = False    # self.isTxState = 'Begin'(establishment phase)일때, No Response 시작되었는가? (시간측정위해 필요)
        self.isNoResposeTransfer = False  # self.isTxState = 'Transfer'(Transfer phase)일때, No Response 시작되었는가? (시간측정위해 필요)

        self.starttimeNoResponseBegin = 0  # No Response 시작시간 저장 (establishment phase)
        self.starttimeNoResponseTransfer = 0  # No Response 시작시간 저장 (Transfer phase)

        self.countTransferNoResponse = 0   #  Transfer phase 에서 No Response count  --- 6회면 종료한다.

        self.lab = []  # 검사결과  저장 리스트
        self.pt = [] # self.lab 에 저장된 값들을 환자별로 분류하여 저장...

        self.framedata = [] # 전송할 frame data (frame 단위로 전송한다)
        self.prevframe =''  # 재전송위한 frame 보관 변수..

        self.postBUNopt = 0  # postBUN option  ; 0 = postBUN 제외, 1= postBUN단독 검사시행.
#        self.grid2List = grid2List   # grid2 검사결과 저장 list  --> grid2 에 표시하고, 이 list에 저장한다.

        # print 'chem   self.grid2List =', self.inst.grid2List

    def getData(self,inst):  # instance를 인수 전달.

        # print 'self.serialData -->',self.serialData

        if self.isSendBtnPressed == True and self.isTxState == 'No' :   # rx mode (idle 상태)에서 처음으로 Send Btn 누른경우
            if(not self.serialData): # 수신된 데이터가 없는 경우에만, 전송모드(send mode)로 진입한다.
                # self.sendmode(inst)
                inst.ser.write(ENQ)  # ENQ 전송
                self.isTxState = 'Begin'  # establishment phase 진입.
            else:
                self.isSendBtnPressed = False
                mylog.msgLog( '수신중인 데이터가 존재하여, 데이터 전송할수없습니다!' )
        elif self.isSendBtnPressed == True and self.isTxState == 'Begin' : # 전송모드(send mode)  establishment phase 인 경우..
            self.sendmode(inst)
        elif self.isSendBtnPressed == True and self.isTxState == 'Transfer' : # 전송모드(send mode)  Transfer phase 인 경우..
            self.transfermode(inst)

        else: self.rxmode(inst)


    def varsInit(self): # send mode & transfer mode 에 필요한 변수들 초기화하여 , rx mode로 셋팅한다.
        self.isSendBtnPressed = False
        self.isTxState = 'No'
        self.countBeginNAK = 0
        self.countTransferNAK = 0  # transfer mode
        self.isNoResposeBegin = False   # establishment phase
        self.isNoResposeTransfer = False    # Transfer phase
        self.starttimeNoResponseBegin = 0
        self.starttimeNoResponseTransfer = 0
        self.countTransferNoResponse = 0   #  Transfer phase 에서 No Response count  --- 6회면 종료한다.
        self.prevframe =''
        self.serialData = ''   # 전송받은 데이터 초기화


    def sendmode(self,inst): # send mode (데이터전송모드)  : establishment phase 인경우

        if self.serialData == ACK: # 정상답변이면 전송모드 시작한다.
            # print 'send next data!'
            self.isTxState = 'Transfer'  # 데이터 전송모드로 전환
            if (len(self.framedata) > 0): # 전송할 frame data 있으면 ..
                self.prevframe = currentframe = self.framedata.pop(0)
                self.sendframe(inst, currentframe)  # frame 데이터 보내기.
                # print ' current frame  ===>', currentframe
            else:
                inst.ser.write(EOT)  # 전송할 frame data 없으면 그냥 종료.
                self.varsInit()

        elif self.serialData == NAK:
            self.countBeginNAK += 1
            if(self.countBeginNAK < 6):
                time.sleep(10)  # 10초 후에 다시 ENQ 보낸다.
                inst.ser.write(ENQ)
            else:
                inst.ser.write(EOT)  # 6번 NAK 이면 tx mode 종료
                self.varsInit()   # 전송모드 관련 변수들 초기화
            # print ' send prev data repeately after 10 secs, max 6 times, then EOT'
        elif self.serialData == ENQ: # ENQ 충돌시, Prestige 우선권으로 인해 rx mode 로 전환한다.
            self.varsInit()   # 전송모드 관련 변수들 초기화
            # print 'change to rx mode d/t Prestige ENQ priority'
            self.isTxState = 'No'  # rx mode 로 전환
        elif self.serialData == EOT: # EOT 답변오면 송신모드 종료!  -- 다시 송신버튼 눌러야함!
            self.varsInit()
        elif self.serialData =='': # No Respose / respose delay
            if(self.isNoResposeBegin == False):
                self.isNoResposeBegin = True    # No Response 시작 설정.
                self.starttimeNoResponseBegin = time.time()  # No Response 시작시간 설정.
            else:
                durationNoResponse = time.time() - self.starttimeNoResponseBegin
                # print 'durationNoResponse ==' , durationNoResponse
                if(durationNoResponse > 15):  # No Response 시간이 15초 경과 되면, 송신 모드(send mode) 종료하고 rx mode로 전환한다.
                    # print ' end of transfer d/t no response!!!!!!!!!!!!!'
                    inst.ser.write(EOT)   # 전송모드 종료
                    self.varsInit()
            # print ' self.starttimeNoResponseBegin = ', self.starttimeNoResponseBegin

            # print ' If no respose for 15 secs, EOT'
        else: # defective respose 인 경우 rx mode 종료함..
            self.varsInit() # 전송모드 종료
            # print ' send prev data repeately , max 6 time , then EOT'

        self.serialData = ''   # 전송받은 데이터 초기화

    def makeSendData(self):  # self.grid2List 데이터를 prestige 로 보낼수있는 data로 가공한다.
        ptlist = []
        code  = dict([(y[0],x) for x,y in self.prescodedict.items()])    # code =  {'C2510': 'UIBC', 'B2710': 'GGT', 'C3795': 'CA', 'C3794': 'PHOS', 'C2443': 'TG', 'C3780': 'UA', 'C2420': 'HDLC', 'C2411': 'TCHOL', 'C3730': 'BUN', 'B2602': 'ALP', 'B2570': 'GOT', 'B2590': 'LDH', 'C3720': 'TBIL', 'C2490': 'FE', 'C3711': 'GLU', 'C3750': 'CREA', 'B2580': 'GPT', 'C2210': 'ALB', 'C2200': 'TP', 'C3721': 'DBIL'}

        startSamplePos = 1  # 샘플  시작 번호  -- 1-30 까지 가능.

        dialog = chemdialog.MyDialog5(None)

        # dialog = wx.TextEntryDialog(None, "샘플 시작번호를 입력하십시요! (1-30)","Sample Postion","1", style = wx.OK|wx.CANCEL)
        if dialog.ShowModal() == wx.ID_OK:
            # print ' yes ok dialog   '
            startSamplePos = int(dialog.dlgTxtCtrl.GetValue()) # 샘플 시작번호
            self.postBUNopt = dialog.dlgRbox.GetSelection() # postBUN option
            # startSamplePos = int(dialog.GetValue())
        else:
            dialog.Destroy()
            return  # 취소시 전송 중단..

        dialog.Destroy()

        # print '\n\n  start sample position  ==== ', startSamplePos

        recorddata = []

        for x in self.inst.grid2List:
            tmp = []
            tmp.append(x[0][2].strip())
            # print 'x === ',x
            # print ' len ----  ', len(x)
            for k in range(1,len(x)):
                # print 'x[k][0].strip()  ====' , x[k][0].strip()
                if x[k][0].strip() ==  'C3730PO' and self.postBUNopt == 0:
                        continue
                elif self.postBUNopt == 1:  # postBUN만 선택하여 검사하는 경우..
                    # prestige에 postBUN 전용 시약 셋팅 안되있으면 , 그냥 BUN 검사를 시해한다.
                    if x[k][0].strip() ==  'C3730PO': tmp.append('BUN')
                    else: continue  # 나머지 검사는 skip!!!
                else:
                    tmp.append(code[x[k][0].strip()])
                    # print 'code[x[k][0].strip()] ---- ', code[x[k][0].strip()]
            if len(tmp) > 1 : ptlist.append(tmp)   # 환자  id 만있고 , lab order code 없으면 skip 한다.

        # print 'ptlist  ==', ptlist

        currenttime = time.strftime("%Y%m%d%H%M%S",time.localtime())
        # print 'current time  =  ', currenttime

        headerrecord = 'H|\^&|||' + self.sender +'|||||' + self.receiver +'||P|1|'+ currenttime +'\r'     # 헤더
        recorddata.append(headerrecord)
        # print 'Header = ', headerrecord

        roundno = 1  # mesurement order record 구성시, samle round number
        dilution = '0'   # mesurement order record 구성시, dilution rate 항목( '0' = no dilution (default) )
        priority ='R'  # mesurement order record 구성시,  ('R' = routine(default), 'S' = STAT)
        actioncode = ''  # mesurement order record 구성시, ('C' = cancel,  'N' =New(default), 'A' = Add, 'Q' = QC)  # option 이므로 사용안해도됨.
        samplekinds = 'Serum'    # mesurement order record 구성시, sample kinds 항목

        for idx, pt in enumerate(ptlist):
            # print idx,pt
            seqNoPt = str( idx+1)   # Seqence No of Patient Infomation Record
            chtno = pt[0]  # chart number
            familyname = chtno  # 차트번호로 대신 사용.
            firstname = chtno   # 차트번호로 대신 사용.
            sex = 'U'   # 성별 ( 'M', 'F' ,'U' = Unknown )

            ptinforecord = 'P|'+ seqNoPt +'|' +chtno+'|||'+familyname+'^'+firstname+'|||'+ sex +'|||||\r'
            recorddata.append(ptinforecord)
            # print 'pt info record = ', ptinforecord

            testitemAll = ''
            seqNoOdr =1   # Sequence No of order record
            sampleId = chtno  # = barcode Id
            samplepos = str(idx+startSamplePos)  # sample position

            for order in range(1,len(pt)):
                # print 'odr --', pt[order], self.itempos.get(pt[order])
                testitem = '^^^' + str(self.itempos.get(pt[order])) + '^' + pt[order] + '^'   + dilution + '\\'
                testitemAll += testitem

            testitemAll = testitemAll[:-1]   # 마지막  '\\' 제저함.
            # print ' test items == ', testitemAll

            odrrecord = 'O|' + str(seqNoOdr) +'|' + sampleId + '|^' + str(roundno) + '^' + samplepos + '|' + testitemAll + '|'  + priority +  '||||||'  + actioncode +  '||||'  + samplekinds + '||||||||||O\r'
            recorddata.append(odrrecord)
            # print 'order record = ', odrrecord
            # print ' len of order record  ====== ' , len(odrrecord)

        terminatorrecord  = 'L|1|N\r'
        recorddata.append(terminatorrecord)

        # print ' ==== record data  == ' , recorddata

        self.framedata = self.makeframedata(recorddata)
        self.isSendBtnPressed = True



    def makeframedata(self,recs):  # record text 를 frame 으로 만든다...
        # print 'records  == ' , recs
        recs240 =[] # record 길이가 240자 넘으면 잘라서 ,  재구성한다..
        countETB = 0 # 240자 넘어서 ETB 를 붙여야 하는 숫자..--> frame 수 계산할때 사용한다.

        for idx, rec in enumerate(recs):
            inc = len(rec)/240
            if (inc == 0):   # 240자 이하인 경우..
                fn = (idx+1+ countETB)%8   # frame number  --  1 부터 시작함.  ( 0 ~ 7 까지 반복함)
                tmp = str(fn) + rec + ETX
                recs240.append(tmp)
            else:    # 240자 초과인 경우..
                for k in range(inc):
                    fn = (idx+1+ countETB)%8
                    tmp = str(fn) + rec[240*k:240*(k+1)] + ETB
                    recs240.append(tmp)
                    countETB += 1
                fn = (idx+1+ countETB)%8
                tmp = str(fn) + rec[240*inc:240*(inc+1)] + ETX
                recs240.append(tmp)

        framedata = []  # 최종 frame 데이터 list

        for rec in recs240:
            # print rec
            chksum = '%02X'%(sum([ ord(x) for x in rec]) & 0xff)  # modulo 256 checksum 만들기
            frame = STX + rec + chksum + '\r\n'    # frame 구조 완성.
            framedata.append(frame)
            # print 'checksum ==', chksum

        # print ' frame data ==', framedata
        return  framedata



    def sendframe(self,inst,frame):  # self.isTxState == 'Transfer' 인 경우..(Transfer phase)
        inst.ser.write(frame)



    def transfermode(self,inst):  # transfer phase 에서 전송한 data frame 에 대한 answer 처리..메소드.
        # print 'self.serialData == ',self.serialData, ' == ',
        if self.serialData != '':
            # print hex(ord(self.serialData))
            pass

        if self.serialData == ACK: # 정상답변이면 다음 frame 전송
            # print ' 다음 frame 전송...'
            if (len(self.framedata) > 0): # 전송할 frame data 있으면 ..
                self.prevframe = currentframe = self.framedata.pop(0)
                self.sendframe(inst, currentframe)  # frame 데이터 보내기.
                # print ' current frame  ===>', currentframe
            else:
                inst.ser.write(EOT)  # 전송할 frame data 없으면  종료.
                self.varsInit()
        elif self.serialData == NAK:
            # print ' NAK ---- '
            self.countTransferNAK += 1

            if(self.countTransferNAK < 6):
                time.sleep(10)  # 10초 후에 다시 ENQ 보낸다.
                self.sendframe(inst,self.prevframe)
            else:
                inst.ser.write(EOT)  # 6번 NAK 이면 tx mode 종료
                self.varsInit()   # 전송모드 관련 변수들 초기화
                # print ' send prev data repeately after 10 secs, max 6 times, then EOT'
        elif self.serialData == EOT: # EOT 답변오면 송신모드 종료!  -- 다시 송신버튼 눌러야함!
            self.varsInit()
        elif self.serialData ==   '':  # No Respose / respose delay
            # print '          no  response  ---- '
            if(self.isNoResposeTransfer == False):
                self.isNoResposeTransfer = True    # No Response 시작 설정.
                self.starttimeNoResponseTransfer = time.time()  # No Response 시작시간 설정.
            else:
                durationNoResponse = time.time()  -  self.starttimeNoResponseTransfer
                # print ' durationNoResponse ==' , durationNoResponse
                if(durationNoResponse > 15):  # No Response 시간이 15초 경과 되면, 송신 모드(send mode) 종료하고 rx mode로 전환한다.
                    inst.ser.write(EOT)   # 전송모드 종료
                    self.varsInit()
            # print ' If no respose for 15 secs, EOT'
        else: # defective respose 인 경우 rx mode 종료함.. (ACK, NAK, EOT 아닌 경우!!!)
            # print '  --- defective response ------ '
            self.countTransferNoResponse  += 1
            if(self.countTransferNoResponse < 6 ):   # 재전송한다...
                # print ' send prev frame '
                pass
            else :
                inst.ser.write(EOT)   # 전송모드 종료
                self.varsInit()

        self.serialData = ''   # 전송받은 데이터 초기화


    def rxmode(self,inst):  # Rx mode (수신모드)
        # print 'serial data ==', self.serialData
        if self.serialData == ENQ:
            self.serialData = self.serialData[:-1]  # 마지막 추가된 ENQ 삭제한다.
            inst.ser.write(ACK)
        elif self.serialData == EOT:
            # print 'end transmission'
            self.serialData = self.serialData[:-1]  # 마지막 추가된 EOT 삭제한다.
            # print  ' self.lab data  출력하기' , ' --- 다음 환자 데이터 입력위해    출력후   환자 데이터  self.lab 에서 지우기..'
            tmppt = []  # self.pt 에 저장하기전 임시 보관 리스트 생성.
            for x in self.lab:
                if(x[2] == 'P'):
                    tmp =  x.split('|')
                    ptid = tmp[2]
                    # print 'chtno  == ',ptid, '  len =', len(ptid)
                    if (len(tmppt)): # 자료있으면
                        self.pt.append(tmppt) # 이전 환자자료를 self.pt 에 저장하고.
                        # print 'self.pt 저장후  ==', self.pt
                        tmppt =[] # 이전 저장환자 자료 지우고   초기화..
                    tmppt.append(ptid)
                elif(x[2] == 'R'):
                    tmp =  x.split('|')
                    odr = tmp[2]
                    val = tmp[3]
                    code = self.prescodedict.get(odr.split('^')[4])
                    if code == None :
                        # print ' 검사항목 = ',  odr.split('^')[4] , ' 정의 안되어 있어 넘어갑니다.'
                        continue  # LDL-chol 처럼  Prestige 장비에서는 검사항목있으나  실제  prescodedict dict 에는 없는 경우  다음 항목으로 넘어가는 처리 구문
                    # print '     order = ',odr.split('^')[4], code[0],' value = ', self.makeNumb(val,code[1])
                    tmppt.append((odr.split('^')[4],code[0],self.makeNumb(val,code[1]) ))

            if(tmppt != []): self.pt.append(tmppt) # 마지막  환자 저장자료를 self.pt 에 저장 , 빈정보는 저장안함.

            self.setrowcnt(self.grid)   # grid  총 row count 구하기
            self.row = -1  # 검사결과 입력될 row 초기화

            # print 'self.lab  == ', self.lab
            # print 'self.pt  ==', self.pt

            if ( self.pt != []): self.displaygrid2(self.pt) # ptlist 를  grid2 화면에 표시 , self.pt data 없으면 skip

            # print 'chem  getData()  self.grid2List =', self.inst.grid2List

            self.lab =[]  # 결과 출력후 초기화
            self.pt = []  # 결과 출력후 초기화
        else:
            isframe = self.checkframe(self.serialData) # tmpframe 이 온전한 frame 인가 조사함.
            if (isframe == 'yes'):
                # print 'save data!!!'
                self.lab.append(self.serialData)
                self.serialData =''
                inst.ser.write(ACK)
            else:
                pass

    def displaygrid2(self,ptlist):  # 정렬된 환자 검사 리스트 ptlist 를  grid2 화면에 표시한다.
        for pt in ptlist:
            self.findEmptyRow(self.grid)  # self.row  설정     #입력받은 grid 에서 비어있는 row 찾기...
            ptid = pt[0]  # 환자 id
            ptrow = self.searchptrow(ptid) # 해당 환자  row 번호
            # print 'pt id & row == ', ptid, ptrow
            if ptrow == -1 : continue  # 해당 환자의  비어있는 row 가 없으면 skip!!!
            labresultcount = len(pt)-1 # 검사 항목 갯수..
            for idx in range(labresultcount):
                ocscode = pt[idx+1][1]
                val = pt[idx+1][2]
                grid2column = self.grid2codematch.get(ocscode)[1]
                if self.postBUNopt == 1 and ocscode == 'C3730':
                    grid2column = 27  # postBUN column 강제 지정...
                    ocscode = 'C3730PO'  # ocs에 저장시에는 다시 post-BUN 코드로 저장한다.
                self.Grid2Value(ptrow, grid2column, val, ocscode)


    def searchptrow(self,ptid): # ptid 에 해당하는 환자 row를 grid2 에서 찾아서 반환한다.(검사일시 비어있는 환자중에서)
        startrow = self.row  #검사일시 비었는 row 부터 마지막row (self.rowcnt -1) 까지 찾는다.
        while (startrow < self.rowcnt):
            gridptid = self.grid.GetCellValue(startrow,2).strip() # grid2 에 표시된 id 공백제거함
            # print "repr(gridptid), repr(ptid) = ", repr(gridptid), repr(ptid)
            if(gridptid == ptid):
                # print ' pt id row  == ', startrow
                return startrow
            elif(self.mkid(gridptid) == ptid):
                # print ' pt 000 id row  == ', startrow
                return startrow
            startrow += 1
        return -1

##    def mkid(self,i):  # grid2 에 표시된 id 에서  앞의 '000' 제거함.
##        i = i.split('-')
##        i[0] = str(int(i[0]))
##        i = '-'.join(i)
##        return i

    def mkid(self, txt):  # grid2 에 표시된 id 에서  앞의 '000' 제거함.
        for idx, x in enumerate(txt):
            if x != '0': return txt[idx:]
        return ''  # all zero

    def checkframe(self,data):
        result = ''
        # print 'checkframe data == ', data
        if (len(data)>6 and (data[-5] == ETX or data[-5] == ETB)):
            # print ' yes ETX', data[0], data[-2:]
            if(data[0] == STX and data[-2:] == '\r\n'):
                result = 'yes'  ## checksum 체크  routine 만들기..
                # print result
        return result

    def makeNumb(self, val, decimal):  # str val 을  소수점 이하 자릿수를  decimal 값으로 맞추어 반환 (검사 결과값 소수자릿수 맞추어 반환..)
        try:
            if(val == ''): return val  # 나중에 자동재검사하는 경우는  ''으로 값넘어온다. -> 모든 검사 끝나고 자동재검사후 최종결과값 넘어온다.
            tmp = float(val)
        except Exception as msg :
            # print ' makeNumb error : val =',val
            mylog.msgLog(msg)
            return val

        if decimal == 0:
            val = '%d' % round(tmp)
        else:
            # print decimal
            val = ('%.' + str(decimal) +'f') % round(tmp,decimal)
            # val =  str(round(tmp,decimal))
        return val  # str type으로 반환

    def setrowcnt(self,grid):  # grid  총 row count 구하기
        self.rowcnt = grid.GetNumberRows()

    def findEmptyRow(self,grid):
        if(self.rowcnt):
            for i in range(self.rowcnt):
                if (grid.GetCellValue(i,1)==''):  # '검사일시' 비어있는 cell 을 찾아서, 처음 발견되는 row 가 결과 입력할 row 이다..!!!!
                    self.row = i
                    break

    def Grid2Value(self,row,col,value,labcode):        # 검사결과를 grid2 화면에 표시하고, self.grid2List 에도 결과값 입력한다.
        wx.CallAfter(self.grid.SetCellValue,row,col,value )
#        self.grid.SetCellValue(row,col,value)
        # print 'self.inst.grid2List[row] ==', self.inst.grid2List[row]
        for i, labdata in enumerate(self.inst.grid2List[row]):   # self.grid2List 해당 row 에서  labcode 있으면 결과값 입력한다.
            if(i != 0):
                if(labdata[0].strip() == labcode ):    # labcode 있으면 결과값 입력한다.
                    labdata[2] = value

        # print 'new self.inst.grid2List[row] =', self.inst.grid2List[row]



#class PrestigeDialog1(chemdialog.MyDialog5):
#    def __init__(self,parent):
#        super(PrestigeDialog1,self).__init__(parent)
#        print '   dialog  class.....'
#    pass



class RS232Viwer(serialtxt.TxtFrame):
    def __init__(self,parent=None):
        serialtxt.TxtFrame.__init__(self,parent)

    def OnCloseTxtFrame( self, event ):
        self.Destroy()
        Form1.RS232Viwerflag = False     #클래스멤버



# log message 저장 및  error 화면 출력.
class MyErrorLog:
    def __init__(self):
        logging.basicConfig(
                            filename = 'laberror.log',
                            format =  '%(asctime)s - %(name)s - %(levelname)s - %(message)s' ,
                            level = logging.DEBUG)
        self.logger = logging.getLogger("master")
#        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    def msgLog(self, errormessage):
        try:
            message = str(errormessage)
        except:
            message  = ''.join(errormessage)  # 메세지 한글깨짐 방지위한 구문..

        wx.MessageBox( message , caption="Notice", style=wx.OK)
        self.logger.info(message)



class OCSdbsetting(dbsetting.DBsetting):
    def __init__(self,parent=None):
        dbsetting.DBsetting.__init__(self, parent)

        conn = sqlite3.connect('lab.db' ,isolation_level=None)  #autocommit
        self.c= conn.cursor()
        sql = """  select * from OCSserver                         """
        self.c.execute(sql)
        self.recs = self.c.fetchall()
#        print self.recs, self.recs[0][5]

        dbengine = self.recs[0][1]
        dbserverip = self.recs[0][2]
        db  = self.recs[0][3]
        uid = self.recs[0][4]

        for i, item in enumerate(self.cho.GetStrings()):
            if (dbengine == item) :
                self.cho.SetSelection(i)   # sqlite 에 저장된값을  wx.choice 에서 선택하여 화면에 표기하라. ( i 번째 목록)

        self.tctServerIP.Value = dbserverip
        self.tctDBname.Value = db
        self.tctUID.Value = uid

    def ocsdbsave( self, event ):

        pwd = hashlib.sha256(self.tctPwd.Value).hexdigest()

        sql = """
                update OCSserver set  serverdb ='%s' , serverip ='%s', dbname='%s' ,uid = '%s'  where no =1
                """

        if (pwd == self.recs[0][5]):
            # print ' ok password match        ---------->  save db !!!!!!!'
            sql = sql %(self.cho.GetLabel(),self.tctServerIP.Value,self.tctDBname.Value, self.tctUID.Value  )
            self.c.execute(sql)
            # print ' 저장되었습니다.'

        else :
            # print ' no password match!!!            --------------> you can\'t save ! '
            pass



    def ocsdbcancel( self, event ):
        self.Destroy()


#===============================================================================
#  sqlite 에   table 만들기
#  - 프로그램 설치시  1회만 실행시키기...
#===============================================================================
class _Makesqlitedb:
    def __init__(self):
        self.conn = sqlite3.connect('lab.db' ,isolation_level=None)  #autocommit
        self.c= self.conn.cursor()

        # self.__makeElectrolyte() # lab db에 electrolyte table 만들기

    #===========================================================================
    #  lab db에 electrolyte table 만들기
    #===========================================================================
    def __makeElectrolyte(self):
        sql = """
        CREATE TABLE [electrolyte] (
              [odrdate] text,
              [examdate] text,
              [chtno] text,
              [name] text,
              [sampleno] text,
              [dept] text,
              [ocmnum] text,

              [Na] text,
              [K] text,
              [Cl] text,

              [rawdata] BLOB);
        """
        self.c.execute(sql)
        self._closeconn(self.conn)

    def _closeconn(self,conn):
        conn.close()


class LabMachine(machinsel.machinsel):  # 장비선택
    def __init__(self,parent):
        machinsel.machinsel.__init__(self, parent)
#        print 'Lab machine class start !!'

        self.conn = sqlite3.connect('lab.db',isolation_level=None) # isolation_level=None ; autocommit
        self.cur = self.conn.cursor()
        self.conn.text_factory = str  # unicode error 해결!!!

        self.chportcbc.Clear()
        self.chportcbc.AppendItems(COMPORT)

        self.chportelectro.Clear()
        self.chportelectro.AppendItems(COMPORT)

        self.chportchem.Clear()
        self.chportchem.AppendItems(COMPORT)



        choices, defaultMachineIndex, defaultComportIndex = self.defaultSetCall('cbc')

        self.chcbc.SetItems(choices)       # choices  -- 선택가능한 장비 리스트
        self.chcbc.SetSelection(defaultMachineIndex)        # defaultMachineIndex  --- default 장비명 인덱스
        self.chportcbc.SetSelection(defaultComportIndex)   # defaultComportIndex --- default comport 인덱스


        choices, defaultMachineIndex, defaultComportIndex = self.defaultSetCall('electro')

        self.chelectro.SetItems(choices)
        self.chelectro.SetSelection(defaultMachineIndex)
        self.chportelectro.SetSelection(defaultComportIndex)


        choices, defaultMachineIndex, defaultComportIndex = self.defaultSetCall('chem')

        self.chchem.SetItems(choices)
        self.chchem.SetSelection(defaultMachineIndex)
        self.chportchem.SetSelection(defaultComportIndex)


    def defaultSetCall(self,machineGroup):
        sql = "select * from machine  where grp = '%s'"      # default 로 선택된 장비 정보가져오기!!!
        self.cur.execute(sql%machineGroup)
        data = self.cur.fetchall()

        choices =[]   # choice 할  장비 리스트
        defaultMachineIndex =0
        defaultComport =''

        for i, rec in enumerate(data):
            choices.append(rec[1])
            if (rec[3]=='Y'):
                defaultMachineIndex =i
                defaultComport =rec[4]

        defaultComportIndex = COMPORT.index(defaultComport)  # comport 이름(예: 'COM1')을 index 로 바꾼다.

        return choices, defaultMachineIndex,defaultComportIndex



class MainF(mainf.MyMainF):
    def __init__(self,parent=None):
        mainf.MyMainF.__init__(self, parent)


    def runCBC( self, event ):
        # print ' -------------------------- cbc clicked'
        cbcform = Form1(None,labmode='cbc')
        cbcform.Show()


    def runElectro( self, event ):
        # print ' ---------------------- electro clicked'
        elecform = Form1(None,labmode='electro')
        elecform.Show()


    def runChem( self, event ):
        # print ' -------------------------chem clicked'
        chemform = Form1(None,labmode='chem')
        chemform.Show()



    def exitProg( self, event ):
        # print ' end'
        self.Destroy()


    def mdbsetting( self, event ):  # menu

        ocsdb = OCSdbsetting(None)
        ocsdb.ShowModal()


    def mmachineset( self, event ):  #menu

        self.labMachine = LabMachine(None)

        rtn = self.labMachine.ShowModal()

        if(rtn == wx.ID_OK) :
            if(self.labMachine.chbcbc.Value):
                self.defaultMachineSet('cbc', self.labMachine.chcbc.GetLabel(), self.labMachine.chportcbc.GetLabel())

            if(self.labMachine.chbelectro.Value):
                self.defaultMachineSet('electro', self.labMachine.chelectro.GetLabel(), self.labMachine.chportelectro.GetLabel())

            if(self.labMachine.chbchem.Value):
                self.defaultMachineSet('chem', self.labMachine.chchem.GetLabel(), self.labMachine.chportchem.GetLabel())
        else:   # 취소시
#            print ' No machine'
            self.labMachine.Destroy()
#            self.Destroy()

        self.labMachine.Destroy()


    def defaultMachineSet(self, group, machine,comport):         # 선택한 장비셋팅을 sqlite db 에 default 로 하여 저장한다.

        sql ="update machine set sel ='N' where grp ='%s'"
        self.labMachine.cur.execute(sql%group)

        sql = "update machine set sel='Y', port ='%s' where grp ='%s' and name = '%s'"
        self.labMachine.cur.execute(sql%(comport,group,machine))


    def mlocaldbview( self, event ):   #menu
        pass





if __name__ == '__main__':

    # _Makesqlitedb()

    app = wx.PySimpleApp()
    frame = MainF(None)
    mylog = MyErrorLog()   # logging instance 형성
    frame.Show()

    app.MainLoop()



