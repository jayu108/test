﻿# -*- coding: euc-kr -*-

'''
Created on 2012. 5. 4.

utf-8 로 저장

@author: jin1
'''

import labdesign
import serialadd1
import serialadd2 
import serialtxt

import wx
import wx.grid
import pyodbc
import datetime
import time
import threading
import serial
import sqlite3
import operator



LABcbc = ['B1050' ,    # WBC 
         'B1040' ,   # RBC
         'B1010' ,    # Hb
         'B1020' ,     # Hct
         'B1020A',    #MCV
         'B1020B',    #MCH
         'B1020C',    #MCHC                   
         'B1060' ,    # PLT

         'B1091A' ,    # neutrophil  (granulocyte)
         'B1091B' ,     # lymphocyte
         'B1091C' ,     # monocyte
         
         'B1220' ,     # PDW
         'B1230',     # RDW
         
         'B1060A',   # PCT
         'B1060B' ,   # MPV
         'B1091' ]    # wbc diff



class Form1 ( labdesign.MyForm ):
    def __init__(self,parent):
        labdesign.MyForm.__init__(self,parent=None)
        
        self.MyStart()

        


    # Virtual event handlers, overide them in your derived class
    
    def OnCloseWindows( self, event ):  # main frame 종료
        
        self.sqconn.close()  # main thread sqlite db close!
        
        self.th1evt.clear()  # th1 thread의 loop를 빠져나온후에...
        self.th1.join()    # thread 종료후                  # 1초후에도 daemon thread 종료되지 않더라도.. 강제로 join() 하라!!  *******
        self.Destroy()    # 프로그램 종료!
        pass
        
    
    def OnBtnSearch( self, event ):  # 조회

        d1 = self.date1.GetValue()
        d2 = self.date2.GetValue()
        m1 = d1.FormatISODate().split('-')
        m2  = d2.FormatISODate().split('-')
        startday=''.join(m1)
        endday = ''.join(m2)

        dday = int(endday)-int(startday)

        if (dday < 0):  # 날짜 순서 잘못 선택시..
#            print ' abnormal range date!!!!!    날짜 선택 다시 하시요!!'
            errdialog1 = serialadd2.MyDialog1(None)
            result = errdialog1.ShowModal()

            if result == wx.ID_OK: errdialog1.Destroy()
#            else: errdialog1.Destroy()
            return  
        

        startday = startday+'0000'     #YYYYMMHH0000  -- 00시 00분
        endday = endday +'2359'      # YYYYMMHH2359  -- 23시 59분
        
#        sql2 = self.sqlsearch2 % (startday,endday)
#                
#        print 'sql2 =', sql2
#        
#        self.grid1List=[]   # grid1 에 표시할 실제 데이터 row로 구성된 list
#        self.grid1List2=[]   # order 번호 추가
#        
#        
#        self.cursor.execute(sql2 )
#        grid1recs = self.cursor.fetchall()
#        print 'grid1recs =', grid1recs
#        print 'len of grid1recs =', len(grid1recs)
#        print ' -------------------------------------------'
        
        self.grid1List2=[]   # gird1 에  표시할 환자정보, 검사코드, order 번호 정보 (기초정보)   
                                # 여기에 검사결과 등을 추가하여 self.grid1List3 만든다.
        
        sqlsearch3 = """select   o.ospodrdtm, o.OspChtNum,p.pbspatnam, o.ospchkstt,o.OspOcmNum,o.OspUidPrt, o.ospodrcod, o.ospodrseq,o.OspDelFlg
                        from OspInf o inner join PbsInf p
                        on o.ospchtnum = p.pbschtnum
                        inner join SeeMst c
                        on o.ospodrcod = c.seeodrcod
                        where  o.ospodrdtm >= '%s'  and o.ospodrdtm <= '%s' and c.seeexpdte ='99999999' and o.ospcsmdep ='LAB' and o.OspDelFlg ='N' and o.OspChkStt = 'A'
                        and ( o.OspOdrCod='B1050' or o.OspOdrCod='B1040' or o.OspOdrCod='B1010' or o.OspOdrCod='B1020' or o.OspOdrCod='B1020A' or o.OspOdrCod='B1020B' or o.OspOdrCod='B1020C' or o.OspOdrCod='B1060' or o.OspOdrCod='B1091A' or o.OspOdrCod='B1091B' or o.OspOdrCod='B1091C' or o.OspOdrCod='B1220' or o.OspOdrCod='B1230' or o.OspOdrCod='B1060A' or o.OspOdrCod='B1060B' or o.OspOdrCod='B1091' )
         order by o.ospodrdtm,o.OspOcmNum, o.OspOdrSeq"""      
           
        sql3 = sqlsearch3 % (startday,endday)
        
        self.cursor.execute(sql3 )
        grid1recs2 = self.cursor.fetchall()    # 쿼리 결과 
#        print 'grid1recs2 =', grid1recs2
#        print 'len of grid1recs2 =', len(grid1recs2)
        
        rectmp =[] # 임시저장, rectmp[0] = ocmnum, 나머지는 (labcode, oder line No) tupule
        # 예:  rectmp = ['    169617', ['B1050     ', 28], ['B1040     ', 29], ['B1010     ', 30], ['B1020     ', 31], ['B1060     ', 32], ['B1220     ', 33], ['B1230     ', 34]]
        
        tmpid=[]  # [0] ; check,    [1]; 처방일,    [2]; 차트번호,     [3]; 이름,     [4]; 상태,       [5];ocmnum,    [6]; 진료과
       
        for rec in grid1recs2:  # 개별 order line 을  정리하여, 환자별로 정리하여 self.grid2List2를 만든다! 
#            print rec[0],rec[1],rec[2],rec[3],rec[4],rec[5],rec[6],rec[7], rec[8]       
             # rec[0] ; 처방일시,    rec[1] ; 차트번호,   rec[2] ; 이름,   rec[4]; ocmnum,   rec[5]; 진료과,   rec[6] ; labcode,   rec[7] ; 오더순서    
            if(rectmp):
                if(rectmp[0][5] != rec[4]):   # ocmnum 가 다르면
                    self.grid1List2.append(rectmp)
                    rectmp= []            # 새로운 record 입력하기.
                    tmpid=[]     # rectmp 비어있으면, 즉 처음시작시
                    tmpid.append('1')   # check
                    tmpid.append(rec[0])  # 처방일
                    tmpid.append(rec[1])    # 차트번호
                    tmpid.append(rec[2])     # name
                    tmpid.append('')   # 진행상황
                    tmpid.append(rec[4])  # ocmnum
                    tmpid.append(rec[5])  # 진료과
                    
                    rectmp.append(tmpid)   # rectmp 비어있으면, 즉 처음시작시                    
                    
#                    rectmp.append(rec[4])
            else:    # rectmp 비어있으면, 즉 처음시작시
                tmpid=[]    
                tmpid.append('1')   # check
                tmpid.append(rec[0])  # 처방일
                tmpid.append(rec[1])    # 차트번호
                tmpid.append(rec[2])     # name
                tmpid.append('')   # 진행상황
                tmpid.append(rec[4])  # ocmnum
                tmpid.append(rec[5])  # 진료과
                
                rectmp.append(tmpid)   # rectmp 비어있으면, 즉 처음시작시
        
            if(len(rectmp)>1):   # 예 :  rectmp = ['    169649', ['B1050     ', 6]]
                for i, data in enumerate(rectmp):
#                    print i, data, data[0]
                    if (i==0):
                        if(data[6] != rec[5]):   # rectmp 에 저장된 dept 부서(data[6])가  현재의 dept (rec[5]) 와 다르다면 , 새로운 rectmp 만들라!
                            self.grid1List2.append(rectmp)  
                            rectmp= []
                            tmpid=[]     # rectmp 비어있으면, 즉 처음시작시
                            tmpid.append('1')   # check
                            tmpid.append(rec[0])  # 처방일
                            tmpid.append(rec[1])    # 차트번호
                            tmpid.append(rec[2])     # name
                            tmpid.append('')   # 진행상황
                            tmpid.append(rec[4])  # ocmnum
                            tmpid.append(rec[5])  # 진료과
                            
                            rectmp.append(tmpid)   # rectmp 비어있으면, 즉 처음시작시                               
                    else:
                        if(data[0] == rec[6]):  # rectmp 에 저장된 labcode (data[0]) 가  현재의 labcode(rec[6]) 와 겹치면, 새로운 rectmp 를 만들어 저장하라.
#                            print '=============double new rec!!!'
                            self.grid1List2.append(rectmp)  
                            rectmp= []
                            tmpid=[]     # rectmp 비어있으면, 즉 처음시작시
                            tmpid.append('1')   # check
                            tmpid.append(rec[0])  # 처방일
                            tmpid.append(rec[1])    # 차트번호
                            tmpid.append(rec[2])     # name
                            tmpid.append('')   # 진행상황
                            tmpid.append(rec[4])  # ocmnum
                            tmpid.append(rec[5])  # 진료과
                            
                            rectmp.append(tmpid)   # rectmp 비어있으면, 즉 처음시작시                              



            tmp = []  # labcode, orderline No  저장 ..(추후 검사 결과추가 저장한다!!!!!)
            tmp.append(rec[6])
            tmp.append(rec[7])
            rectmp.append(tmp)
#            print 'len of rectmp =',len(rectmp), ': rectmp =', rectmp
#        print 'last rectmp =' , rectmp
        if(rectmp) : # 마지막 rectmp 에 data가 있으면 
            self.grid1List2.append(rectmp)    #마지막 record 저장하기.
        
#        print 'grid1List2 =', self.grid1List2
        
        self.grid1List3 =[]  # 초기화,    쿼리 결과  grid1recs2 에  cbc결과, 진행상황등을 추가하여 grid1 data 완성한 List!!!!
        
        if(self.grid1List2):  # self.grid1List3 만드는 부분
            for i,rec in enumerate(self.grid1List2):
                sqlcbc2 = """ select  ResLabCod,ResOdrSeq, ResRltVal  from ResInf
                                    where resocmnum = '%s' 
                                and ( """                
#                print i, rec[0][3], rec[0][5], 'length=', len(rec), ' : ' , rec
                for k in range(1,len(rec)):
                    sqlcbc2 = sqlcbc2 + ' ResOdrSeq = \''  +  str(rec[k][1]) +'\'  or'
                sqlcbc2 = sqlcbc2[:-2]  +')'    # sqlcbc2 끝의 'or' 문자 제거후 ')' 덮기.
                sqlcbc2 = sqlcbc2 %rec[0][5]         # rec[0][5]  == ocmnum
#                print 'sqlcbc2 =', sqlcbc2
                self.cursor.execute(sqlcbc2 )
                cbcrecs2 = self.cursor.fetchall()    # cbcrecs2 ; ('cbc코드', 'order번호', '결과값') 으로 구성된 tupule 의 List

                
#                if (len(cbcrecs2) != (len(rec) -1) ): print 'labcode count mismatch!!!!!'     
#                else: print ' labcode count match!-------------'    

                cbctmp2 =[]  # 결과값 임시저장
                

                for data in cbcrecs2:
                    if (data[0].strip() =='B1091'):   # data[0].strip()  --> 공백 없애기.,   diff 면 건너 뛰어라!      # data[0] = 검사코드
                        continue  
                    cbctmp2.append(data[2])  
                              
                if(cbctmp2.count(None)==0): 
                    rec[0][4] = '검사완료'
                elif(len(cbctmp2) == cbctmp2.count(None)): 
                    rec[0][4] = '검사대기'
                else: 
                    rec[0][4] = '부분완료'       
                    
#                print 'rec[0] =', rec[0]
                newrec =[]
                newrec.append(rec[0])
#                print 'rec[0][4] =',   rec[0][4] 
        
                self.grid1List3.append(newrec+cbcrecs2)   # grid1 데이터 집어넣기
        
        
        
        if(self.RbList.GetSelection() == 0):             # 검사완료자제외 선택시....
            for i, rec in reversed(list(enumerate(self.grid1List3))):    # 역순으로  뒤에것부터 삭제하기 위함.
#                print i, rec
                if (rec[0][4] =='검사완료'):
#                    print rec[0][4]
                    del self.grid1List3[i]   # 검사완료자 delete!
        
#        print ' new   self.grid1List3 = ', self.grid1List3  # '검사완료자제외' 선택시에는 부분완료, 검사대기 만 포함된다...
#        
#
#        
#        print 'sql3 =', sql3
#        print ' =====================================  '      
        



        recsCount = len(self.grid1List3)   # records count
        
        self.gridPrep(self.grid1,recsCount)



        for i, rec in enumerate(self.grid1List3):            # grid1 에  데이터 표시하기!!!!
#            print rec[0][0], rec[0][1], rec[0][2], rec[0][3],rec[0][4],rec[0][5], rec[0][6]
            self.grid1.SetCellValue(i,0,rec[0][0])   # check 표시
            self.grid1.SetCellValue(i,1,rec[0][1][:8])  # 처방일만 표시  ---   'YYYYMMDDHHMM' 중 앞 8자만 표시..
            self.grid1.SetCellValue(i,2,rec[0][2])
            self.grid1.SetCellValue(i,3,rec[0][3])
            self.grid1.SetCellValue(i,4,rec[0][4])
            self.grid1.SetCellValue(i,5,rec[0][5])     #  rec[4] --> ocmnum 내원번호
            self.grid1.SetCellValue(i,6,rec[0][6])     # 진료과    
            
            
            
        self.attr2 = wx.grid.GridCellAttr()
        self.attr2.SetReadOnly()
        self.grid1.SetColAttr(1,self.attr2)  # 읽기전용으로 속성 변환.
        self.grid1.SetColAttr(2,self.attr2)
        self.grid1.SetColAttr(3,self.attr2)
        self.grid1.SetColAttr(4,self.attr2)
        self.grid1.SetColAttr(5,self.attr2)
        self.grid1.SetColAttr(6,self.attr2)
        
        self.grid1.AutoSizeColumn(2)        
        
        self.sortflag = [-2,False]  # grid1 정렬위한 flag, 초기화 ,,,  self.sortflag[0] --> column 번호, self.sortflag[1] --> 역순정렬여부
        
        
    
    
            
    def gridPrep(self, grid, NewRowCount):  # # NewRowCount 만큼  grid row 를 줄이거나 늘려라!!
        CurrentRowCount = grid.GetNumberRows()   # 현재 row count
        
        if CurrentRowCount > NewRowCount:
            grid.DeleteRows(0,CurrentRowCount - NewRowCount)  # 차이만큼 Row 삭제
        elif CurrentRowCount < NewRowCount:
            grid.InsertRows(0,NewRowCount - CurrentRowCount)   # 차이만큼 Row 삽입 하여  record count 만큼 row count 증가시킨다.        
    
    
    
    def MyListRange( self, event ):
        pass
    
    def OnBtnAllSel( self, event ): # 전체 선택
        if (self.grid1.GetNumberRows()):  # grid1 에 data가 있으면  다음을 실행.        
            for i in range(self.grid1.GetNumberRows()):  # grid1 의 row No. 0 부터 마지막 row No. 까지
                self.grid1.SetCellValue(i,0,'1')   # 모든 row 를 check 하기 !!        
        for rec in self.grid1List3:
            print rec
    
    def OnBtnAllCan( self, event ):  # 전체 취소
        if (self.grid1.GetNumberRows()):  # grid1 에 data가 있으면  다음을 실행.        
            for i in range(self.grid1.GetNumberRows()):  # grid1 의 row No. 0 부터 마지막 row No. 까지
                self.grid1.SetCellValue(i,0,'')   # 모든 row 를 check 해제 하기 !!          
        
    
    def OnBtnRev( self, event ):  # 반전.
        if (self.grid1.GetNumberRows()):  # grid1 에 data가 있으면  다음을 실행.        
            for i in range(self.grid1.GetNumberRows()):  # grid1 의 row No. 0 부터 마지막 row No. 까지
                if(self.grid1.GetCellValue(i,0)) :  # check 되어 있으면
                    self.grid1.SetCellValue(i,0,'')   # check 해제 하기 !!
                else:  self.grid1.SetCellValue(i,0,'1')     # check 해제 되어있으면 , check 하기...     
        
    
    def OnGridLtClick( self, event ):  # grid1   left click
#        print 'click'

        if event.Col == 0:
            wx.CallLater(100,self.toggleCheckBox)

#            self.rowcheck(event.GetRow())
#            print ' self.cb = ', self.cb
#            print 'self.cb value = ', self.cb.GetValue
#            print 'self.cb value = ', self.cb.Value

        event.Skip()
    
#    def rowcheck(self, row):
#        print 'row =',row

    
    def OnGridLtDClick( self, event ):
        
        grid1row = event.GetRow()  # double click 된 grid1 row No.
        
        ocmnum = self.grid1.GetCellValue(grid1row,5)   #  내원번호
        self.SendList(ocmnum, grid1row)
        
        event.Skip()
   
    
    def onEditorCreated( self, event ):          # self.grid1 을 시작할때. self.grid1.EnableEditing( True ) 로 되어있어야 한다!!!!  *****
#        print 'editor created'
        if event.Col == 0:
            self.cb = event.Control
            self.cb.WindowStyle |= wx.WANTS_CHARS
            self.cb.Bind(wx.EVT_KEY_DOWN,self.onGridKeyDown)
            self.cb.Bind(wx.EVT_CHECKBOX, self.onCheckBoxPrint)   # CheckBox 객체의 event 연결.
        event.Skip()
    
    def onCheckBoxPrint(self,event):
#        print 'checked!!!!!! ' ,event.IsChecked(), event.GetEventObject()
        self.myCheck(event.IsChecked())
        
    def OnG1LabelClick( self, event ):  # grid1 label click  for 정렬
        row = event.GetRow()
        col = event.GetCol()
#        print 'row =', row
#        print 'col =', col
#        
#        print 'self.sortflag =' , self.sortflag[0], self.sortflag[1]
        
        if (col == self.sortflag[0]): self.sortflag[1] = not self.sortflag[1]   # self.sortflag[1]  :  정렬 reverse 옵션 ---  True or  False 
        else: 
            self.sortflag[0] = col     # 정렬할 column
            self.sortflag[1] = False

#        print 'sort list =',  self.grid1List3
#        for i, rec in enumerate(self.grid1List3):
#            print i,rec[0][col],  rec
        
#        print ' ==================='
        if ((col >=0) and(self.grid1.GetNumberRows()>0)):
            self.grid1List3 = sorted(self.grid1List3, key = lambda rec:rec[0][col], reverse=self.sortflag[1])  # 클릭한 label 순서로 정렬하여 self.grid1List3에 저장
            
#            for i, rec in enumerate(self.grid1List3):
#                print i,rec[0][col],  rec
            for i, rec in enumerate(self.grid1List3):            # grid1 에  데이터 표시하기!!!!
    #            print rec[0][0], rec[0][1], rec[0][2], rec[0][3],rec[0][4],rec[0][5], rec[0][6]
                self.grid1.SetCellValue(i,0,rec[0][0])   # check 표시
                self.grid1.SetCellValue(i,1,rec[0][1][:8])  # 처방일만 표시  ---   'YYYYMMDDHHMM' 중 앞 8자만 표시..
                self.grid1.SetCellValue(i,2,rec[0][2])
                self.grid1.SetCellValue(i,3,rec[0][3])
                self.grid1.SetCellValue(i,4,rec[0][4])
                self.grid1.SetCellValue(i,5,rec[0][5])     #  rec[4] --> ocmnum 내원번호
                self.grid1.SetCellValue(i,6,rec[0][6])     # 진료과           

    
    
    def onCellSelected( self, event ):    
        if event.Col == 0:
            wx.CallAfter(self.grid1.EnableCellEditControl)
        event.Skip()

    
    
    def OnBtnSend( self, event ):    # 선택환자 검사하기 버튼 
        if (self.grid1.GetNumberRows()):  # grid1 에 data가 있으면  다음을 실행.        
            for i in range(self.grid1.GetNumberRows()):  # grid1 의 row No. 0 부터 마지막 row No. 까지 ocmnum 를 찾아서 해당 cbc 항목, 결과 전송하라.
                if (self.grid1.GetCellValue(i,0)):   # check 되어있는 항목만 선택하여 보내라!!
                    ocmnum = self.grid1.GetCellValue(i,5)
                    grid1row = i
                    self.SendList(ocmnum, grid1row)
                    
#        for rec in self.grid1List3:
#            print rec
            

    def OnBtnCancelGrid2( self, event ):  # 선택취소 버튼, grid2
#        print self.grid2.GetGridCursorRow()   # click 된 grid2 row 값 반환 함수.        
        if(self.grid2.GetNumberRows()):
            if(self.grid2.GetSelectionBlockTopLeft()):  # drag로 범위 설정시
                TLt = self.grid2.GetSelectionBlockTopLeft()
                BRt = self.grid2.GetSelectionBlockBottomRight()
#                print TLt, BRt
#                print ' TLt[0] -- ', TLt[0][0]
                self.grid2.DeleteRows( TLt[0][0] , BRt[0][0]-TLt[0][0]+1)   # grid2 화면에서도 지우고
                del self.grid2List[TLt[0][0]:BRt[0][0]+1]   # grid2 와 연동된 self.grid2List 에서도 해당부분 삭제함.

            elif(self.grid2.GetSelectedCells()):   # Ctrl+click 으로 여러 cell 선택시
                selcells =self.grid2.GetSelectedCells()   # 선택순서대로 좌표값입력..
#                print ' selcells  -- ', selcells 
                selcells.sort(reverse=True)    # # 뒤집어서 큰 좌표부터 정렬  ---> 삭제시 문제 안생김.
#                print ' reverse ;  ', selcells      
                for i in selcells:
                    self.grid2.DeleteRows(i[0],1)   # i[0]  --->  선택된 row     # grid2 화면에서도 지우고
                    del self.grid2List[i[0]]             # grid2 와 연동된 self.grid2List 에서도 해당부분 삭제함.
            else:  
                DelRow = self.grid2.GetGridCursorRow()
                self.grid2.DeleteRows(DelRow,1)   # 한개의 cell에 click 된 경우.    # grid2 화면에서도 지우고
                del self.grid2List[DelRow]             # grid2 와 연동된 self.grid2List 에서도 해당부분 삭제함.


    
    def OnBtnCancelAllGrid2( self, event ):    # 전체 취소 , grid2
        if(self.grid2.GetNumberRows()):
            self.grid2.DeleteRows(0,self.grid2.GetNumberRows())   # grid2 화면에서도 지우고
        self.grid2List =[]    # grid2 와 연동된 self.grid2List 에서도 삭제함.
        
    def OnGrid2LtClick( self, event ):
        self.grid3.SetCellValue(0,1,self.grid2.GetCellValue(event.GetRow(),0) )   # 처방일 복사하기.
        self.grid3.SetCellValue(1,1,self.grid2.GetCellValue(event.GetRow(),1) )    # 검사일시 복사
        
        # 추후에는 sqlite db 에 저장한 값을 가져와서 grid3에 출력할것 ( 이전 검사 포함하여)****
        for i in range(15):   
            self.grid3.SetCellValue(i+2,1,self.grid2.GetCellValue(event.GetRow(),i+7) )

        event.Skip()


    
    def OnBtnSave( self, event ):   # 서버 MSSQL db 에 저장하기.

        sql = """update  ResInf
                 set ResRltVal = '%s' 
                 where resocmnum = '%s'  and ResOdrSeq = '%s' """


        for i,data in enumerate(self.grid2List):   # grid2List 에 있는 모든 환자 자료 저장 ( 검사시행여부 상관없이) 
#            print i, 'ocmnum =', data[0][6], 'exam date =', data[0][1], 'data =', data
            for k, lab in enumerate(data):
                if(k !=0): 
#                    print lab
                    if (lab[2] != None):  # lab 결과값이 None 아니면 저장하라!
#                        print ' data  -- ', lab[1],lab[2]
                        updsql = sql %( lab[2], data[0][6],lab[1] )
#                        print 'updsql =', updsql
                        self.cursor.execute(updsql)


        

    def Menudbview( self, event ):  # 메뉴 ; 보기 - 저장결과보기 (local sqlite3 db 에 저장된 결과보기)
                   
        self.dbview = serialadd1.MyFrame2(None)        
        self.dbview.Show()

        self.sqcur.execute("""select odrdate , examdate , chtno , name ,
                sampleno , dept , ocmnum ,
                wbc , rbc , hb , hct , mcv , mch , mchc ,
                plt , neutro , lymph , mono ,
                pdw , rdw , pct , mpv,rawdata  from cbc""")
        recs = self.sqcur.fetchall()
        
        rowcount  = len(recs)
        
        self.gridPrep(self.dbview.grid21, rowcount)
        
        for i,rec in enumerate(recs):
            for j,value in enumerate(rec):
#                print i, j, value
                if( value == None): value ='<null>'    # 이 줄 없으면, TypeError: coercing to Unicode: need string or buffer, NoneType found 발생!!!!
                val = unicode(value,'utf-8').encode('euc-kr')
                self.dbview.grid21.SetCellValue(i,j,val)

    
    def RS232TextView( self, event ):  # 메뉴 ; 보기 - 전송자료모두보기
        self.txtviewer = RS232Viwer(None)
        self.RS232Viwerflag  = True
        self.txtviewer.Show()

    
    
    def toggleCheckBox(self):
#        print 'toggle1'
#        print ' toggle  1st  cb.val = ', self.cb.Value
        self.cb.Value = not self.cb.Value
#        print 'toggle self.cb.val =', self.cb.Value   
        self.myCheck(self.cb.Value)
         
    
    def myCheck(self,IsChecked):  # self.grid1List3 정보 수정!!
#        print 'check = ', self.grid1.GridCursorRow, IsChecked
        
        if(IsChecked): 
            self.grid1List3[self.grid1.GridCursorRow][0][0] ='1'
        else: self.grid1List3[self.grid1.GridCursorRow][0][0] =''
        
#        print self.grid1List3[self.grid1.GridCursorRow][0][0] , self.grid1List3[self.grid1.GridCursorRow][0][3]
#        for i,data in enumerate(self.grid1List3):
#            print i, data[0][3], data[0][0]
    
    def onGridKeyDown(self,evt):
        if evt.KeyCode == wx.WXK_UP:
            if self.grid1.GridCursorRow > 0:
                self.grid1.DisableCellEditControl()
                self.grid1.MoveCursorUp(False)
        elif evt.KeyCode == wx.WXK_DOWN:
            if self.grid1.GridCursorRow < (self.grid1.NumberRows-1):
                self.grid1.DisableCellEditControl()
                self.grid1.MoveCursorDown(False)
        elif evt.KeyCode == wx.WXK_LEFT:
            if self.grid1.GridCursorCol > 0:
                self.grid1.DisableCellEditControl()
                self.grid1.MoveCursorLeft(False)
        elif evt.KeyCode == wx.WXK_RIGHT:
            if self.grid1.GridCursorCol < (self.grid1.NumberCols-1):
                self.grid1.DisableCellEditControl()
                self.grid1.MoveCursorRight(False)
        else:
            evt.Skip()    
    
    
    def MyStart(self):
        self.attr = wx.grid.GridCellAttr()
        self.attr.SetEditor(wx.grid.GridCellBoolEditor())
        self.attr.SetRenderer(wx.grid.GridCellBoolRenderer())
        self.grid1.SetColAttr(0,self.attr)
        self.grid1.SetColSize(0,20)        
        
        self.sqconn = sqlite3.connect('lab.db',isolation_level=None) # isolation_level=None ; autocommit
        self.sqcur = self.sqconn.cursor()
        self.sqconn.text_factory = str  # unicode error 해결!!!
        
        self.conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.10.200;DATABASE=DrBITPACK;UID=sa;PWD=bit', autocommit=True )
        self.cursor = self.conn.cursor()    
        
        self.grid1List3 =[]  # grid1 에 표시할 실제 데이터 row로 구성된 list       
                                # self.grid1List3[0] = 환자정보,           [1],[2],.... = (cbc labcode, order seq, value) 튜플 
                                # [0][0] = 체크여부, [0][1] = 오더일, [0][2]=차트번호,  [0][3]= 이름,  [0][4] = 진행상황, [0][5] = ocmnum,  [0][6] = dept
                                 
        self.grid2List=[]   # grid2 에 표시할 실제 데이터 row로 구성된 list
                                # self.grid2List[0] = 환자정보,           [1],[2],.... = (cbc labcode, order seq, value) 튜플 
                                # [0][0] = 처방일, [0][1] = 검사일시, [0][2]=차트번호,  [0][3]= 이름,  [0][4] = 검체번호, [0][5] =dept ,  [0][6] =  ocmnum

       
       
        # 검사실 넘어온 order 중 '완료환자'로 처리된  CBC with diff 항목있는 환자만 체크하는 SQL   
        
        # OspDelFlg ='N'  ---> 삭제 되지 않은 환자만 보여주기        
        #  (OspChkStt = 'C' --> 검사실 대기환자, 'A' --> 검사실 완료환자, 'H' --> 검사실 대기환자)
        # 기간별 search
        self.sqlsearch2 =   ''' select distinct  o.ospodrdtm, o.OspChtNum,p.pbspatnam, o.ospchkstt,o.OspOcmNum,o.OspUidPrt
                from OspInf o inner join PbsInf p
                on o.ospchtnum = p.pbschtnum
                inner join SeeMst c
                on o.ospodrcod = c.seeodrcod
                where  o.ospodrdtm >= '%s' and o.ospodrdtm <= '%s' and c.seeexpdte ='99999999' and o.ospcsmdep ='LAB' and o.OspDelFlg ='N' and o.OspChkStt = 'A'
                and ( '''
                
        cbcsql = ''
                
        for i, cbc1 in enumerate(LABcbc):
            cbcsql = cbcsql + 'o.OspOdrCod=' + '\'' + cbc1 + '\' or '  
   
        cbcsql =  cbcsql[:-3] + ')  \n order by o.ospodrdtm '

#        self.sqlsearch = self.sqlsearch + cbcsql
        self.sqlsearch2 = self.sqlsearch2 + cbcsql



        cbcsql =''
        
        # 해당일 환자의 ocmnum 에 해당하는  CBC c diff 검사한 결과 가져오기...
        self.sqlcbc = ''' select  ResLabCod, ResRltVal  from ResInf
                            where resocmnum = '%s' 
                        and (
                        '''   
        
        for i, cbc1 in enumerate(LABcbc):
            cbcsql = cbcsql + 'ResLabCod=' + '\'' + cbc1 + '\' or '    
        
        self.sqlcbc = self.sqlcbc + cbcsql[:-3] +')'
        
#        self.cbcDict={}   # ocmnum 을 key 로해서 CBC c diff 결과값 저장.  ****     
#        self.cbcDict2 ={}     # 초기화, order 번호 추가함.

        self.grid3.SetCellValue(0,0,'처방일')
        self.grid3.SetCellValue(1,0, '검사일시')
        
        self.grid3.SetCellValue(2,0, 'WBC')
        self.grid3.SetCellValue(3,0, 'RBC')
        self.grid3.SetCellValue(4,0, 'Hb')
        self.grid3.SetCellValue(5,0, 'Hct')
        self.grid3.SetCellValue(6,0, 'MCV')
        self.grid3.SetCellValue(7,0, 'MCH')    
        self.grid3.SetCellValue(8,0, 'MCHC')
        self.grid3.SetCellValue(9,0, 'Platelet')
        self.grid3.SetCellValue(10,0, 'Neutrophil')
        self.grid3.SetCellValue(11,0, 'Lymphocyte')
        self.grid3.SetCellValue(12,0, 'Monocyte')
        self.grid3.SetCellValue(13,0, 'PDW')         
        self.grid3.SetCellValue(14,0, 'RDW')
        self.grid3.SetCellValue(15,0, 'PCT')
        self.grid3.SetCellValue(16,0, 'MPV')    
        
        
        self.RS232Viwerflag  = False  #  RS232 text viewer flag초기화.
        self.sortflag = [-2,False]  # grid1 정렬위한 flag, 초기화     ;  [0] = column number,   [1] = reverse 상태(True, False)
        
        self.th1 = threading.Thread(target=self.OnRS232)
#        print 'thread made'
        self.th1evt = threading.Event()
        self.th1.setDaemon(1)
        self.th1evt.set()
        self.th1.start()
                 
    
    def SendList(self, ocmnum, grid1row):  # grid1 row 에 해당하는 ocmnum 의 검사항목, 결과값을 grid2 로 보낸다!
        
        ptinfo=[]  # grid2List[0] 에 사용할  환자정보 임시보관 list
                        # [0] = 처방일, [1] = 검사일시, [2]=차트번호,  [3]= 이름,  [4] = 검체번호, [5] =dept , [6] =  ocmnum
                        
        rectmp =[]   #  grid2 환자 record  정보 
                     
#        print 'self.grid1List3[grid1row] =', self.grid1List3[grid1row]
        ptinfo.append(self.grid1List3[grid1row][0][1])    # [0] = 처방일
        ptinfo.append('')                                     # [1] = 검사일시
        ptinfo.append( self.grid1List3[grid1row][0][2])   # [2]=차트번호
        ptinfo.append( self.grid1List3[grid1row][0][3])   # [3]= 이름
        ptinfo.append('')                                      #  [4] = 검체번호
        ptinfo.append(self.grid1List3[grid1row][0][6])   #  [5] =dept
        ptinfo.append(self.grid1List3[grid1row][0][5] )   # [6] =  ocmnum
#        print 'ptinfo =', ptinfo
        
        if(len(self.grid2List) != 0) :   # 데이터 있으면 , 중복인지 검사!!
            for data in self.grid2List:
                if(data[0][6] == ocmnum):
                    if(data[0][5] ==self.grid1List3[grid1row][0][6]):   # dept 가 같은가?
                        if(data[1][1] ==self.grid1List3[grid1row][1][1] ):  # 처음 labcode 의 order seq 중복인가
                            return 
        
     
        rectmp.append(ptinfo)
        
        for i, data in enumerate(self.grid1List3[grid1row]):  # 선택환자의 labcode , order seq, value  정보를 가져와서 rectmp 에 추가한다.
#            print i, data
            if(i != 0) :
                rectmp.append(list(data))   # 검사결과값 입력위해 list 로 형변환.
#                print data[0]
                
#        print  'rectmp =',   rectmp
        self.grid2List.append(rectmp)  # 선택한 환자를 self.grid2List 에 넣는다.
        
#        print 'self.grid2List =', self.grid2List
#        print ' length of self.grid2List =', len(self.grid2List)
        
        
        self.grid2.AppendRows(1)

        AddedRow = self.grid2.GetNumberRows() -1            

        self.grid2.SetCellValue(AddedRow,0,self.grid2List[AddedRow][0][0])  # 처방일
        self.grid2.SetCellValue(AddedRow,2,self.grid2List[AddedRow][0][2])  # 차트번호
        self.grid2.SetCellValue(AddedRow,3,self.grid2List[AddedRow][0][3])  # 이름   
        self.grid2.SetCellValue(AddedRow,5,self.grid2List[AddedRow][0][5])  # 진료과   
        self.grid2.SetCellValue(AddedRow,6,self.grid2List[AddedRow][0][6])  # 내원번호

        for i, data in enumerate(rectmp):   #검사결과 gird2 에 출력, None 이면 'V' 로 표시..
            if(i != 0) :
                for k, code in enumerate(LABcbc[:-1]):     # LABcbc[:-1]  --> diff 는 뺀다...
                    if(data[0].strip() == code):  
#                        print code, ' == match!! =' ,  k+7
                        if(data[2]) : 
                            self.grid2.SetCellValue(AddedRow, k+7, data[2])
                        else:
                            self.grid2.SetCellValue(AddedRow, k+7, 'V')    # 결과값이 없으면 grid2 cell 에   'V' 를 입력하라.



    def OnRS232(self):
#        print 'start'
        
        try : 
            ser = serial.Serial(0)  #com1 port 연결, Open port 0 at “9600,8,N,1”, no timeout:
            ser.timeout=0.5   # timeout 설정  = 0.5초
        except : 
            print ' COM1 포드 열수 없습니다.'
            time.sleep(1)
            self.Destroy() 
            print ' expire'   
            return        
        
        cbc = ''  # cbc buffer
        
        self.thconn = sqlite3.connect('lab.db',isolation_level=None) # isolation_level=None ; autocommit
        self.thcur = self.thconn.cursor()
        self.thconn.text_factory = str  # unicode error 해결!!!        
        
        while(self.th1evt.isSet()):

##            print time.ctime()
            labdata = ser.read(1)              # thread를 daemon으로 하지않으면  값 들어올때 까지 프로그램 정지!!!! 
            tt=[]

            if(labdata ):
                n = ser.inWaiting()
                if(n):
                    labdata = labdata + ser.read(n)
                    cbc= cbc+labdata
            
            if(len(cbc)>100):
                rowcnt = self.grid2.GetNumberRows()
                if((cbc[0]=='\x02')and (cbc[-1] =='\x03')):
                    labtime = str(datetime.datetime.now())  # 문자열로 바꾸어 timestamp 저장. (검사일시)
                    row= -1  # 검사결과 입력될 row 초기화
                    if(rowcnt):
                        for i in range(rowcnt):
                            if (self.grid2.GetCellValue(i,1)==''):  # '검사일시' 비어있는 cell 을 찾아서, 처음 발견되는 row 가 결과 입력할 row 이다..!!!!
#                                print ' total row count =',rowcnt
#                                print 'detected row ='  , i
                                row = i
                                break
#                        else:
#                            print 'no detected empty row !!!!!'

#                    print ' -=================='
#                    if (cbc[0]=='\x02'): print 'STX'
#                    if (cbc[-1] =='\x03'): print 'ETX'
#                    print '데이터 길이 =', int(cbc[1:6])
#                    print ' +++++++++++++++++++++++++++++'
##                    print cbc
#                    print ' ========================= '
                    ttt= cbc.split('\r')
                    
                    if (row != -1):   # 결과입력 할 row 가 있으면
    #                    print ttt
                        for i, data in enumerate(ttt):
#                            print 'line No = ',i, '  ; data =  ',data
                            if (i >= 9 and i <= 26):
                                try:
                                    val = str(float(data[2:7]))
                                except:
                                    pass
                            
#                            if (i ==5): print int(data[2:6]) , '  lab count'  # 1일 총측정수 (검체번호)
                            if (i==9): 
#                                print float(data[2:7]) , '  wbc'
#                                self.grid2.SetCellValue(row,7,str(float(data[2:7])))
#                                val = str(float(data[2:7]))
                                labcode = 'B1050' 
                                self.Grid2Value(row,7, val, labcode)
                                
                            if (i==10): 
#                                print float(data[2:7]) , '  rbc'
#                                self.grid2.SetCellValue(row,8,str(float(data[2:7])))
#                                val = str(float(data[2:7]))
                                labcode = 'B1040' 
                                self.Grid2Value(row,8, val, labcode)                                
                            if (i==11): 
#                                print float(data[2:7]) , '  Hb'
#                                val = str(float(data[2:7]))
                                labcode = 'B1010' 
                                self.Grid2Value(row,9, val, labcode)

                            if (i==12): 
#                                print float(data[2:7]) , '  Hct'
#                                self.grid2.SetCellValue(row,10,str(float(data[2:7])))
                                labcode = 'B1020' 
                                self.Grid2Value(row,10, val, labcode)                                
                            if (i==13): 
#                                print float(data[2:7]) , '  MCV'
#                                self.grid2.SetCellValue(row,11,str(float(data[2:7])))
                                labcode = 'B1020A' 
                                self.Grid2Value(row,11, val, labcode)                                  
                            if (i==14): 
#                                print float(data[2:7]) , '  MCH'
#                                self.grid2.SetCellValue(row,12,str(float(data[2:7])))
                                labcode = 'B1020B' 
                                self.Grid2Value(row,12, val, labcode)                                  
                            if (i==15): 
#                                print float(data[2:7]) , '  MCHC' 
#                                self.grid2.SetCellValue(row,13,str(float(data[2:7])))
                                labcode = 'B1020C' 
                                self.Grid2Value(row,13, val, labcode)                                  
                            if (i==16): 
#                                print float(data[2:7]) , '  RDW'
#                                self.grid2.SetCellValue(row,19,str(float(data[2:7])))
                                labcode = 'B1230' 
                                self.Grid2Value(row,19, val, labcode)                                  
                            if (i==17): 
#                                print int(data[2:7]) , '  PLT'
#                                self.grid2.SetCellValue(row,14,str(int(data[2:7])))
                                labcode = 'B1060' 
                                self.Grid2Value(row,14, val, labcode)                                  
                            if (i==18): 
#                                print float(data[2:7]) , '  MPV'
#                                self.grid2.SetCellValue(row,21,str(float(data[2:7])))
                                labcode = 'B1060B' 
                                self.Grid2Value(row,21, val, labcode)                                  
                            if (i==19): 
#                                print float(data[2:7]) , '  PCT'
#                                self.grid2.SetCellValue(row,20,str(float(data[2:7])))
                                labcode = 'B1060A' 
                                self.Grid2Value(row,20, val, labcode)                                 
                            if (i==20): 
#                                print float(data[2:7]) , '  PDW'
#                                self.grid2.SetCellValue(row,18,str(float(data[2:7])))
                                labcode = 'B1220' 
                                self.Grid2Value(row,18, val, labcode)                                 
                            if (i==21): 
#                                print float(data[2:7]) , '  Lymphocyte' 
#                                self.grid2.SetCellValue(row,16,str(float(data[2:7])))      
                                labcode = 'B1091B' 
                                self.Grid2Value(row,16, val, labcode)                                                       
                            if (i==22): 
#                                print float(data[2:7]) , '  monocyte'
                                self.grid2.SetCellValue(row,17,str(float(data[2:7])))
                                labcode = 'B1091C' 
                                self.Grid2Value(row,17, val, labcode)                                      
                            if (i==23): 
#                                print float(data[2:7]) , '  granulocyte'
#                                self.grid2.SetCellValue(row,15,str(float(data[2:7])))
                                labcode = 'B1091A' 
                                self.Grid2Value(row,15, val, labcode)                                      
#                            if (i==24): print float(data[2:7]) , '  lym#'
#                            if (i==25): print float(data[2:7]) , '  mon#'
#                            if (i==26): print float(data[2:7]) , '  gran#' 
                        # grid2 에 데이터 보내기...
                        self.grid2.SetCellValue(row,1,labtime[:-7])
                        
#                        print 'col No =', self.grid2.GetNumberCols()
                        
                        tmprec=[]
                        
                        for col in range(self.grid2.GetNumberCols()) :
#                            print self.grid2.GetCellValue(row,col)
                            tmprec.append(self.grid2.GetCellValue(row,col) )
                        
                        cbchex = cbc.encode('hex')
                        tmprec.append(cbchex)
                        sql = '''insert into cbc (odrdate , examdate , chtno , name ,
                                sampleno , dept , ocmnum ,
                                wbc , rbc , hb , hct , mcv , mch , mchc ,
                                plt , neutro , lymph , mono ,
                                pdw , rdw , pct , mpv,rawdata )    
                                values (?,?,?,?,?,  ?,?,?,?,?,  ?,?,?,?,?,  ?,?,?,?,?,  ?,?,?) '''   # 로컬 sqlite3에 저장한다.
                
                        self.thcur.execute(sql,tmprec)                        
                        
#                    print 'inserted row =', row
                    # sqlite3 cbc.db 에 저장

                    if(self.RS232Viwerflag):   #메뉴 - '전송자료모두보기' 에 자료 보내기
#                        print 'self.RS232Viwerflag =', self.RS232Viwerflag
                        data = cbc.decode('ascii','ignore')         # UnicodeDecodeError  에러 무시하기 위해 'ignore' 옵션 적용!!!!
                        self.txtviewer.RS232TxtCtrl.AppendText(data)   
                        self.txtviewer.RS232TxtCtrl.AppendText('============================== END =========================\n')

                    cbc ='' # cbc buffer 초기화

                    
        if(ser.isOpen()):
            ser.close()    # close port
            
#        print 'End RS232 thread================'
        
        self.thconn.close()   # close thread sqlite db
        
        
        
    def Grid2Value(self,row,col,value,labcode):        # 검사결과를 grid2 화면에 표시하고, self.grid2List 에도 결과값 입력한다.
        self.grid2.SetCellValue(row,col,value)
#        print self.grid2List[row]
        for i, labdata in enumerate(self.grid2List[row]):   # self.grid2List 해당 row 에서  labcode 있으면 결과값 입력한다. 
            if(i != 0):
#                print labdata[0].strip()
                if(labdata[0].strip() == labcode ):    # labcode 있으면 결과값 입력한다. 
#                    print ' yes mathc!!!'
#                    print ' old data =', labdata[2]
#                    print 'labdata = ', value         
                    labdata[2] = value
#        print 'new self.grid2List[row] =', self.grid2List[row]
        pass
            
            
            
    def Sql(self,sql):
        self.thcur.execute(sql)
        
        
    def MakeDb(self): # 최초 설치시 cbc.db 만들기 
        conn = sqlite3.connect('lab.db')
        c= conn.cursor()
        c.execute(''' create table cbc
                        (odrdate text, examdate text, chtno text, name text,
                        sampleno text, dept text, ocmnum text,           
                        wbc text, rbc text, hb text, hct text, mcv text, mch text, mchc text,
                        plt text, neutro text, lymph text, mono text,
                        pdw text, rdw text, pct text, mpv text, rawdata BLOB )'''  )    #  dept ; 진료과
        
        #  sampleno  --> 검체번호, examno  ---> 검사차수
        conn.close()
        pass


class RS232Viwer(serialtxt.TxtFrame):
    def __init__(self,parent=None):
        serialtxt.TxtFrame.__init__(self,parent)

    def OnCloseTxtFrame( self, event ):
        self.Destroy()
        frame.RS232Viwerflag = False
#        print 'end txt viewer'
#        pass
    

if __name__ == '__main__':
    app = wx.PySimpleApp()
    frame = Form1(None)
    frame.Show()
       
    app.MainLoop()